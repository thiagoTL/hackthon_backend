const mongoose = require('mongoose');
const { mockRequest, mockResponse } = require('../src/tests/util/Mocks')

const recommendationController = require('../src/app/controllers/RecommendationController');

const validRecommendation = { 
    stars: 5,
    content: "Esse profissional é muito bom!",
    client_picture: false,
}

test('Should return successful message', ()=>{
    let req = mockRequest();
    req.body = validRecommendation;
    
    const res = mockResponse();

    console.log(recommendationController.index(req, res));
})