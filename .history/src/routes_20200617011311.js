const { Router } = require("express");
const multer = require("multer");

// controllers
const Professional = require("./app/controllers/UserProfessionalController");
const Investor = require("./app/controllers/InvestorController");
const Articles = require("./app/controllers/ArticleController");
const UserController = require("./app/controllers/UserController");
const SessionUserController =  require("./app/controllers/SessionUserController");
const RecommendationController = require("./app/controllers/RecommendationController");

const GenerateCode = require("./app/controllers/GenerateCode");
const CertificatesController = require("./app/controllers/CertificatesController");

//middlewares
const authMiddlewares = require("./app/middlewares/auth");
const multerConfig = require("./config/multer");

const routes = new Router();

const uploads = multer(multerConfig);


//user auth
routes.post("/register", UserController.store);
routes.post("/login", SessionUserController.store);

// Código de confirmação de número
routes.post("/code/confirm", GenerateCode.confirm);
routes.post("/code/resend", GenerateCode.resend);

// middleware de autenticação
routes.use(authMiddlewares);

//profissional
routes.get("/professionals", Professional.index);
routes.post("/professionals/qualifications", Professional.show);
routes.delete("/professional/remove", Professional.remove);

// artigos
routes.get("/articles", Articles.index);
routes.post("/articles/find", Articles.show);
routes.post("/articles/create", Articles.create);
routes.delete("/articles/remove", Articles.remove);
routes.put("/articles/update", Articles.update);
routes.put("/articles/like", Articles.like);

// cliente
routes.get("/clients", Investor.index);
routes.get("/client/:id", Investor.show);
routes.delete("/clients/remove", Investor.remove);
routes.put("/clients/update", Investor.update);

//recomendação de cliente
routes.get("/recomms", RecommendationController.index);
routes.post('/recomms', RecommendationController.store);
routes.delete("/recomms/remove", RecommendationController.remove);
routes.put("/recomms/update", RecommendationController.update);
routes.post("/recomms/find", RecommendationController.show); //Ainda não funciona
routes.delete("/recomms/removeAllByProfessional", RecommendationController.removeAllProfessionalEvaluation);

//certificados
routes.put("/certificates", CertificatesController.update);

module.exports = routes;
