const { Router } = require("express");
const multer = require("multer");

// controllers
const Professional = require("./app/controllers/UserProfessionalController");
const Investor = require("./app/controllers/InvestorController");
const Articles = require("./app/controllers/ArticleController");
const UserController = require("./app/controllers/UserController");
const SessionUserController =  require("./app/controllers/SessionUserController");
const RecommendationController = require("./app/controllers/RecommendationController");
// const UserProfessionalController = require("./app/controllers/")

// const GenerateCode = require("./app/controllers/GenerateCode");

const GenerateCode = require("./app/controllers/GenerateCode");
const CertificatesController = require("./app/controllers/CertificatesController");

//middlewares
const authMiddlewares = require("./app/middlewares/auth");
const multerConfig = require("./config/multer");

const routes = new Router();

const uploads = multer(multerConfig);


//user auth
routes.post("/register", UserController.store);
routes.post("/login", SessionUserController.store);
// Código de confirmação de número
routes.post("/confirm", GenerateCode.show);

// middleware de autenticação
routes.use(authMiddlewares);

//profissional
routes.get("/professionals", Professional.index);
routes.post("/professionals", Professional.show);
routes.delete("/professional", Professional.remove);

// artigos
routes.get("/articles", Articles.index);
routes.post("/articles/find", Articles.show);
routes.post("/articles/create", Articles.create);
routes.delete("/articles/remove", Articles.remove);
routes.put("/articles/update", Articles.update);
routes.put("/articles/like", Articles.like);

// cliente
routes.get("/clients", Investor.index);
routes.get("/clients/:id", Investor.show);
routes.delete("/clients/remove", Investor.remove);
routes.put("/clients/update", Investor.update);

//recomendação de cliente
routes.get("/recomms", RecommendationController.index);
routes.post('/recomms', RecommendationController.store);
routes.delete("/recomms/remove", RecommendationController.remove);
routes.put("/recomms/update", RecommendationController.update);
routes.get("/recomms/:id", RecommendationController.show);
routes.delete("/recomms/removeAllByProfessional", RecommendationController.removeAllProfessionalEvaluation);

// routes.post("/confirm", GenerateCode.show);

//certificados
routes.put("/certificates", CertificatesController.update);

module.exports = routes;
