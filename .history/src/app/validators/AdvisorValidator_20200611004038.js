const yup = require("yup");

const AdvisorValidatorSchema = yup.object().shape({
    aboutMe: yup
        .string()
        .max(250)
        .notRequired(),
    yearsExperience: yup
        .number()
        .min(0)
        .max(100)
        .notRequired(),
    accessor: yup
        .boolean()
        .notRequired(),
    brokerCompany: yup
        .string()
        .notRequired()
        .min(2)
        .max(50),    
    nativeLanguage: yup
        .string()
        .required()
        .max(50),
});

module.exports = AdvisorValidatorSchema;
