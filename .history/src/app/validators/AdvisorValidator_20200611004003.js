const yup = require("yup");

const AdvisorValidatorSchema = yup.object().shape({
    aboutMe: yup
        .string()
        .max(250)
        .notRequired(),
    yearsExperience: yup
        .number()
        .min(0)
        .max(100)
        .notRequired(),
    accessor: yup
        .boolean()
        .notRequired(),
    brokerCompany: yup
        .string()
        .notRequired()
        .min(2)
        .max(50),    
    nativeLanguage: yup
        .string()
        .required()
        .max(50),
    password: yup
        .string()
        .length(8)
        .matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]/) //pelo menos uma letra e um numero
        .required(),
    confirmedPassword: yup
        .string()
        .oneOf([yup.ref('password'), null], 'The Passwords must match')
        .required(),
    dob: yup
        .date()
        .required()
        .min(new Date(1900, 0, 1))
        .max( generateMaximumDateToBeOverEighteen()),
    linkedin: yup
        .string()
        .url()
        .notRequired(),
    country: yup
        .string()
        .required(),
    agreeTerms: yup
        .boolean()
        .required(),
    verifiedSms: yup
        .boolean()
        .required(),
    verifiedIdDocument: yup
        .boolean()
        .notrequired(),
});

module.exports = AdvisorValidatorSchema;
