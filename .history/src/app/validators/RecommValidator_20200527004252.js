const yup = require("yup");

client:{
    type: Schema.Types.ObjectId,
    ref:'NewClientSchema',
    required: true,
},
professional:{
    type: Schema.Types.ObjectId,
    ref:'NewProfessionalSchema',
    required: true,
},
content: {
    type: String,
    maxlength: 250,
},
stars: {
    type: Number,
    required: true,
    default: 0,
    min: 0,
    max: 5,
},
client_picture: {
    type: Boolean,
    default: false,
},
CreatedAt: {
    type: Date,
    default: Date.now,
},

const RecommValidatorSchema = yup.object().shape({
    client: yup
        .required(),
    professional: yup
        .required(),
    content: yup
        .string()
        .max(250)
        .required(),
    stars: yup
        .number()
        .required()
        .min(0)
        .max(5),
    client_picture: yup
        .boolean()
        .default(false)
        .required(),
});

module.exports = RecommValidatorSchema;
