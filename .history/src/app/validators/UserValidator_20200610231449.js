const yup = require("yup");

function getMaxDate() { 
    const today = new Date();
    const maxDate = new Date((today.getFullYear() - 18), today.getMonth(), today.getDate())
    return maxDate;
}

const UserValidatorSchema = yup.object().shape({
    userType: yup
        .string()
        .required(),
    fisrtName: yup
        .string()
        .min(2)
        .max(100)
        .required(),
    lastName: yup
        .string()
        .min(2)
        .max(100)
        .required(),
    email: yup
        .string()
        .email()
        .required(),
    confirmedEmail: yup
        .string()
        .oneOf([yup.ref('email'), null], 'The E-mail must match')
        .required(),    
    mobile: yup
        .number()
        .required()
        .min(8),
    password: yup
        .string()
        .length(8)
        .matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]/) //pelo menos uma letra e um numero
        .required(),
    confirmedPassword: yup
        .string()
        .oneOf([yup.ref('password'), null], 'The Passwords must match')
        .required(),
    dob: yup
        .date()
        .required()
        .min(new Date(1900, 0, 1))
        .max( getMaxDate()),
    linkedin: yup
        .string()
        .url()
        .notRequired(),
    country: yup
        .string()
        .required(),
    agreeTerms: yup
        .boolean()
        .required(),
    verifiedSms: yup
        .boolean()
        .required(),
    verifiedIdDocument: yup
        .boolean()
        .notrequired(),
});

module.exports = UserValidatorSchema;


