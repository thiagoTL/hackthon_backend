const yup = require("yup");

const RecommValidatorSchema = yup.object().shape({
    client: yup
        .string()
        .required(),
    professional: yup
        .string()
        .required(),
    content: yup
        .string()
        .max(250)
        .notRequired(),
    stars: yup
        .number()
        .required()
        .min(0)
        .max(5),
    client_picture: yup
        .boolean()
        .default(false)
        .required(),
});

module.exports = RecommValidatorSchema;

const RecommValidatorTst = yup.object().shape({
    content: yup
        .string()
        .max(250)
        .notRequired(),
    stars: yup
        .number()
        .required()
        .min(0)
        .max(5),
    client_picture: yup
        .boolean()
        .default(false)
        .required(),
});

module.exports = RecommValidatorTst;