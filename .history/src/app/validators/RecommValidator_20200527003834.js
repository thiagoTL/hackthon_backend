const yup = require("yup");

const ClientValidatorSchema = yup.object().shape({
    firstName: yup
        .string()
        .min(2)
        .max(100)
        .required(),
    lastName: yup
        .string()
        .min(2)
        .max(100)
        .required(),
    email: yup
        .string()
        .email()
        .required(),
    mobile: yup
        .number()
        .required()
        .min(8),
    password: yup
        .string()
        .length(8)
        .matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]/) //pelo menos uma letra e um numero
        .required(),
    confirmPassword: yup
        .string()
        .oneOf([yup.ref('password'), null], 'The Passwords must match')
        .required(),
    birthDate: yup
        .date()
        .required()
        .min(new Date(1900, 0, 1))
        .max( getMaxDate()),
    linkedin: yup
        .string()
        .url()
        .notRequired(),
    country: yup
        .string()
        .required(),
    urls: yup
        .string()
        .notRequired(),
});

module.exports = ClientValidatorSchema;

function getMaxDate() { 
        const today = new Date();
        const maxDate = new Date((today.getFullYear() - 18), today.getMonth(), today.getDate())
        return maxDate;
}
