const RecommendationSchema = require('../models/Recommendation');
const ProfessionalSchema = require('../models/NewProfessional');

function addGradeCalc(avaliacao, stars) {
    const sum = avaliacao.sum + stars
    const qtde = avaliacao.quantity + 1

    const final = sum / qtde

    return {
        avaliacao: {
            sum: sum,
            quantity: qtde,
            rating: final,
        }
    }
}

function removeGradeCalc(avaliacao, stars) {
    const sum = avaliacao.sum - stars
    const qtde = avaliacao.quantity - 1

    const final = sum / qtde

    return {
        avaliacao: {
            sum: sum,
            quantity: qtde,
            rating: final,
        }
    }
}

function updateGradeCalc(avaliacao, oldStars, newStars) {
    const sum = avaliacao.sum - oldStars + newStars
    const qtde = avaliacao.quantity

    const final = sum / qtde

    return {
        avaliacao: {
            sum: sum,
            quantity: qtde,
            rating: final,
        }
    }
}

module.exports = {
    async store(req, res) {
        const client_id = req.userId
        console.log(client_id)

        const professional_id = req.body.professional

        console.log(professional_id)

        const { content, stars, client_picture } = req.body


        newEvaluation = {
            client: client_id,
            professional: professional_id.id,
            content,
            stars,
            client_picture
        }

        console.log(newEvaluation);
        var evaluation;

        try {
            evaluation = await RecommendationSchema.create(newEvaluation);
            console.log(evaluation);

        } catch (error) {
            return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + error.message });
        }

        var professional;
        if (evaluation != null) {
            try {
                professional = await ProfessionalSchema.findById({ _id: professional_id.id });
                console.log(professional)
            } catch (error) {
                return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + error.message });
            }

            if (professional != null) {

                const newAvaliacao = addGradeCalc(professional.avaliacao, stars);

                ProfessionalSchema.findByIdAndUpdate({ _id: professional_id.id }, newAvaliacao, function (err, result) {
                    if (err) return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + error.message });
                    console.log("The professional rating was updated");

                });

                res.send("Recommendation register and professional rating update was performed successfully.")

            } else {
                return res.send('An error has ocurred. Please, try again. :( Professional null.');
            }
        } else {
            return res.send('An error has ocurred. Please, try again. :( Recommendation null.');
        }
    },

    async update(req, res) {

        const { _id, content, stars } = req.body

        console.log(_id);

        newValues = {
            content,
            stars,
            client_picture
        }

        console.log(newValues);
        var evaluation;

        try {
            evaluation = await RecommendationSchema.findByIdAndUpdate(_id, newValues);
            console.log(evaluation);

        } catch (error) {
            return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + error.message });
        }

        var professional;
        if (evaluation != null) {
            try {
                professional = await ProfessionalSchema.findById(evaluation.professional);
                console.log(professional)
            } catch (error) {
                return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + error.message });
            }

            if (professional != null) {

                console.log(professional.avaliacao);

                const newAvaliacao = updateGradeCalc(professional.avaliacao, evaluation.stars, stars);

                ProfessionalSchema.findByIdAndUpdate(evaluation.professional, newAvaliacao, function (err, result) {
                    if (err) return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + err.message });
                    console.log("The professional rating was updated");

                });

                res.send("Recommendation and professional rating update was performed successfully.")

            } else {
                return res.send('An error has ocurred. Please, try again. :( Professional null.');
            }
        } else {
            return res.send('An error has ocurred. Please, try again. :( Recommendation null.');
        }
    },
    async remove(req, res) {

        const { _id } = req.body

        console.log(_id);

        var evaluation;

        try {
            evaluation = await RecommendationSchema.findByIdAndRemove(_id);
            console.log(evaluation);

        } catch (error) {
            return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + error.message });
        }

        var professional;
        if (evaluation != null) {
            try {
                professional = await ProfessionalSchema.findById(evaluation.professional);
                console.log(professional)
            } catch (error) {
                return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + error.message });
            }

            if (professional != null) {

                console.log(professional.avaliacao);

                const newAvaliacao = removeGradeCalc(professional.avaliacao, evaluation.stars);

                ProfessionalSchema.findByIdAndUpdate(evaluation.professional, newAvaliacao, function (err, result) {
                    if (err) return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + err.message });
                    console.log(result);
                });

                res.send("Recommendation remove and professional rating update was performed successfully.")

            } else {
                return res.send('An error has ocurred. Please, try again. :( Professional null.');
            }
        } else {
            return res.send('An error has ocurred. Please, try again. :( Recommendation null.');
        }
    },
    async index(req, res) {

        RecommendationSchema.find({}, function (err, evaluations) {
            if (err) return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + err.message });

            if (evaluations.length == 0) {
                res.send("No recommendation found.");
            }
            return res.send({ evaluations });

        })
    },

    async show(req, res) {

        const tags = req.body;
        try {
            const data = await RecommendationSchema.find(tags).populate('user');
            return res.send({ data });
        } catch (err) {
            return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + err.message });
        }
    },
    /* async showTheLatestThreeByProfessional(req, res) {

        const { professional_id } = req.body

        console.log(professional_id);

        const data = await RecommendationSchema.find(professional_id).sort({ created_at: -1 }).limit(3).exec(function (err, post) {
            if (err) return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + err.message });
            return res.send({ data });
        });


    } */

}