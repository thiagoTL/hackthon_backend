const ClientSchema = require("../models/NewClient");
const ProfessionalSchema = require("../models/NewProfessional");
const twilio = require("twilio");

const client = twilio(
  process.env.TWILIO_ACCOUNT_SID,
  process.env.TWILIO_AUTH_TOKEN
);

function generateToken(params = {}) {
  return jwt.sign(params, authConfig.secret, {
    expiresIn: 86400,
  });
}

class GenerateCode {
  async show(req, res) {
    try {
      const { code, phone } = req.body;

      const codigo = code;
      const numero = phone;

      client.verify
        .services(process.env.TWILIO_SERVICE_SID)
        .verificationChecks.create({ to: numero, code: codigo })
        .then((verification_check) => res.json(verification_check));
      return res.json({
        message: "Successfully approved code",
      });
    } catch (error) {
      return res.json({ message: "Error approved generate code" });
    }
  }
}

module.exports = new GenerateCode();
