const UserSchema = require("../models/User");
const UserValidator = require("../validators/UserValidator")


const twilio = require('twilio');

const investor = twilio(
    process.env.TWILIO_ACCOUNT_SID,
    process.env.TWILIO_AUTH_TOKEN
);

class UserController {
  async create(req, res) {

    UserValidator.validate(req.body).catch(function(err) {
      return res.status(400).send({ error: `${err.name}: ${err.errors}`});
    });

    const { email, mobile, userType } = req.body;

    try {
      if (await UserSchema.findOne({ email })) {
        return res.status(400).send({ error: "E-mail is already registered." });
      }
      if (await UserSchema.findOne({ mobile })) {
        return res.status(400).send({ error: "Mobile number is already registered." });
      }

      //TODO verificar se o create deve ficar depois do envio e confirmação por SMS
      const user = await UserSchema.create(obj);

      user.password = undefined; //Não sei o pq disso

      console.log("User registered successfully");

      //Envio do sms
      const numero = req.body.mobile;
          investor.verify.services(process.env.TWILIO_SERVICE_SID)
          .verifications
          .create({to: numero, channel: 'sms'})
          .then(verification => console.log(verification.status));
      
          if(userType === "investor"){

          }

          if(userType === "advisor"){
              
          }
      
          
      return res.json({ user: user });

    } catch (err) {
      console.log(err);
      return res
        .status(400)
        .send({ error: "An error has ocurred. Please, try again. :(" });
    }
  }
};

module.exports = new UserController();