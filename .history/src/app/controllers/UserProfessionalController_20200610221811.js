const NewProfessionalSchema = require("../models/NewProfessional");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const authConfig = require("../../config/auth");
const twilio = require("twilio");


const client = twilio(
   process.env.TWILIO_ACCOUNT_SID,
   process.env.TWILIO_AUTH_TOKEN
);



function generateToken(params = {}) {
  return jwt.sign(params, authConfig.secret, {
    expiresIn: 86400,
  });
}

module.exports = {
  async store(req, res) {
    console.log("RES", req.body);
    const { email, phone } = req.body;

    try {
      if (await NewProfessionalSchema.findOne({ email })) {
        return res.status(400).send({ error: "email já cadastrado " });
      }
      if (await NewProfessionalSchema.findOne({ phone })) {
        return res.status(400).send({ error: "telefone já cadastrado " });
      }

      const { location: urls = "" } = req.file;

      const obj = {
         ...req.body,
         urls,
      };

      const data = await NewProfessionalSchema.create(req.body);

      data.password = undefined;

      console.log("Usuário criado com sucesso!");

      const numero = req.body.phone;
      
      client.verify
        .services(process.env.TWILIO_SERVICE_SID)
        .verifications.create({ to: numero, channel: "sms" })
        .then((verification) => console.log(verification.status));
      

      return res.json(data);
    } catch (err) {
      console.log(err);
      return res
        .status(400)
        .send({ error: "Ocorreu algum erro, tente novamente :( " });
    }
  },

  async index(req, res) {
    try {
      const data = await NewProfessionalSchema.find().sort({avaliacao: "desc"});

      console.log("RET", req.userId);
      if (data.length == 0) {
        res.send("Nenhum profissional cadastrado.");
      }
      return res.send({ data });
    } catch (err) {
      return res.status(400).send({ error: "Algo deu errado :(" });
    }
  },

  async show(req, res) {
    const certificate = req.body;
    try {
      const response = await NewProfessionalSchema.find(certificate);

      return res.json(response);
    } catch (error) {
      return res.status(400).send({ error: "Algo deu errado :(" });
    }
  },

  async remove(req, res) {
    const { _id } = req.body;
    try {
      const Professional = await NewProfessionalSchema.findByIdAndRemove({
        _id,
      });
      return res.send("Removido");
    } catch (err) {
      console.log(err);
      return res.send("error");
    }
  },
};
