const RecommendationSchema = require('../models/Recommendation');
const ProfessionalSchema = require('../models/NewProfessional');
const RecommendationValidator = require("../validators/RecommValidator");


function addGradeCalc(avaliacao, stars) {
    const sum = avaliacao.sum + stars
    const qtde = avaliacao.quantity + 1

    const final = sum / qtde

    return {
        avaliacao: {
            sum: sum,
            quantity: qtde,
            rating: final,
        }
    }
}

function removeGradeCalc(avaliacao, stars) {
    const sum = avaliacao.sum - stars
    const qtde = avaliacao.quantity - 1

    const final = sum / qtde

    return {
        avaliacao: {
            sum: sum,
            quantity: qtde,
            rating: final,
        }
    }
}

function updateGradeCalc(avaliacao, oldStars, newStars) {
    const sum = avaliacao.sum - oldStars + newStars
    const qtde = avaliacao.quantity

    const final = sum / qtde

    return {
        avaliacao: {
            sum: sum,
            quantity: qtde,
            rating: final,
        }
    }
}

module.exports = {
    async store(req, res) {
        console.log('create recommendation');
        const client_id = req.userId

        console.log('client_id')
        console.log(client_id)

        const { professional_id, content, stars, client_picture } = req.body

        console.log('body')
        console.log(req.body);

        newEvaluation = {
            client: client_id,
            professional: professional_id,
            content: content,
            stars: stars,
            client_picture: client_picture
        }
        console.log('newEvaluation')
        console.log(newEvaluation);
        try {
            
            await RecommendationValidator.validate(newEvaluation);
            const evaluation = await RecommendationSchema.create(newEvaluation);
            console.log('newEvaluation')
            console.log(evaluation);
            if (evaluation != null) {
                const professional = await ProfessionalSchema.findOne({ _id: professional_id });
                console.log('professional.avaliacao')
                console.log(professional.avaliacao);

                if (professional != null) {
                    const newAvaliacao = await addGradeCalc(professional.avaliacao, stars);
                    console.log('nova avalaicao do profissional')
                    console.log(newAvaliacao);

                    const professionalUpdated = await ProfessionalSchema.findOneAndUpdate({ _id: professional_id }, newAvaliacao)
                    console.log("The professional rating was updated");
                    console.log(professionalUpdated.avaliacao);
                    res.send({
                        evaluation,
                        professionalUpdated,
                    })
                }
            }
        } catch (err) {
            return res.json({ error: `${err.name}: ${err.message}` });
        }
    },

    async update(req, res) {

        const { _id, client_picture, content, stars } = req.body
        //const { _id, updatedData } = req.body;


        newValues = {
            content,
            stars,
            client_picture,
            client,
            professional
        }

        console.log(newValues);

        try {
            await RecommendationValidator.validate(newValues);
            const evaluation = await RecommendationSchema.findOneAndUpdate(_id, newValues);
            console.log("The evaluation was updated");
            console.log('' + evaluation);
            if (evaluation != null) {
                const professional = await ProfessionalSchema.findById(evaluation.professional);

                if (professional != null) {
                    console.log(professional.avaliacao);

                    const newAvaliacao = await updateGradeCalc(professional.avaliacao, evaluation.stars, stars);

                    const professionalUpdated = await ProfessionalSchema.findOneAndUpdate(evaluation.professional, newAvaliacao);

                    console.log("The professional rating was updated");
                    res.send({
                        evaluation,
                        professionalUpdated,
                    })
                }
            }
        } catch (err) {
            return res.json({ error: `${err.name}: ${err.message}` });
        }

        /* RecommendationValidator.validate(newValues).catch(function (err) {
             return res.status(400).send({ error: `${err.name}: ${err.errors}` });
         });
 
         console.log(newValues);
         var evaluation;
 
         try {
             evaluation = await RecommendationSchema.findByIdAndUpdate(_id, newValues);
             console.log(evaluation);
 
         } catch (error) {
             return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + error.message });
         }
 
         var professional;
         if (evaluation != null) {
             try {
                 professional = await ProfessionalSchema.findById(evaluation.professional);
                 console.log(professional)
             } catch (error) {
                 return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + error.message });
             }
 
             if (professional != null) {
 
                 console.log(professional.avaliacao);
 
                 const newAvaliacao = updateGradeCalc(professional.avaliacao, evaluation.stars, stars);
 
                 ProfessionalSchema.findByIdAndUpdate(evaluation.professional, newAvaliacao, function (err, result) {
                     if (err) return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + err.message });
                     console.log("The professional rating was updated");
 
                 });
 
                 res.send("Recommendation and professional rating update was performed successfully.")
 
             } else {
                 return res.send('An error has ocurred. Please, try again. :( Professional null.');
             }
         } else {
             return res.send('An error has ocurred. Please, try again. :( Recommendation null.');
         }*/
    },
    async remove(req, res) {

        const _id = req.body

        console.log(_id);

        try {
            const evaluation = await RecommendationSchema.findOne(_id);
            console.log(evaluation);
            await RecommendationSchema.remove(evaluation);
            if (evaluation != null) {
                const professional = await ProfessionalSchema.findOne(evaluation.professional);
                console.log(professional)

                if (professional != null) {
                    console.log(professional.avaliacao);

                    const newAvaliacao = removeGradeCalc(professional.avaliacao, evaluation.stars);

                    const professionalUpdated = ProfessionalSchema.findOneAndUpdate(evaluation.professional, newAvaliacao);

                    console.log("Recommendation remove and professional rating update was performed successfully.")
                    res.send({
                        evaluation,
                        professionalUpdated,
                    })



                }
            }

        } catch (error) {
            return res.status(400).send({ error: 'An error has ocurred. Please, try again. :( =>' + error.message });
        }

        /*   var professional;
           if (evaluation != null) {
               try {
                   professional = await ProfessionalSchema.findById(evaluation.professional);
                   console.log(professional)
               } catch (error) {
                   return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + error.message });
               }
   
               if (professional != null) {
   
                   console.log(professional.avaliacao);
   
                   const newAvaliacao = removeGradeCalc(professional.avaliacao, evaluation.stars);
   
                   ProfessionalSchema.findByIdAndUpdate(evaluation.professional, newAvaliacao, function (err, result) {
                       if (err) return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + err.message });
                       console.log(result);
                   });
   
                   res.send("Recommendation remove and professional rating update was performed successfully.")
   
               } else {
                   return res.send('An error has ocurred. Please, try again. :( Professional null.');
               }
           } else {
               return res.send('An error has ocurred. Please, try again. :( Recommendation null.');
           }*/
    },
    async index(req, res) {

        try {
            const data = await RecommendationSchema.find().populate('user');
            return res.send({ data });
        } catch (err) {
            return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + err.message });
        }
    },

    async show(req, res) {

        const tags = req.params;
        try {
            const data = await RecommendationSchema.find(tags).populate('user');
            return res.send({ data });
        } catch (err) {
            return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + err.message });
        }
    },

    async removeAllProfessionalEvaluation(req, res) {

        const professional = req.body;

        const cleanEvaluation = { avaliacao: { sum: 0, quantity: 0, rating: 0 } }

        console.log({professional: professional._id});

        res.send({msg:'isso'})

      /*  try {
        const professional = req.params;
            const professionalUpdated = await ProfessionalSchema.findOneAndUpdate(professional, cleanEvaluation);

            await RecommendationSchema.deleteMany({professional);

            return res.send(professionalUpdated);
        } catch (err) {
            return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + err.message });
        }*/
    },

}