const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const twilio = require("twilio");
const UserSchema = require("../models/User");
const authConfig = require("../../config/auth");

const client = twilio(
  process.env.TWILIO_ACCOUNT_SID,
  process.env.TWILIO_AUTH_TOKEN
);

function generateToken(params = {}) {
  return jwt.sign(params, authConfig.secret, {
    expiresIn: 86400,
  });
}

class GenerateCode {
  async confirm(req, res) {
    try {
      const { code, mobile } = req.body;

      const codigo = code;
      const numero = mobile;

      client.verify
        .services(process.env.TWILIO_SERVICE_SID)
        .verificationChecks.create({ to: numero, code: codigo })
        .then((verification_check) => res.json(verification_check));
        
        const user = await UserSchema.findOneAndUpdate({ mobile }, {verifiedSms: true}, {new: true});

        return res.json({
        message: "Successfully approved code",
        user: user,
        token: generateToken({ id: user.id }),
      });
    } catch (error) {
      return res.json({ message: "Error generate code" });
    }
  }

  async resend(req, res) {
    const { mobile } = req.body;
    try {
      const numero = mobile;

      console.log("NUERO", numero)

      client.verify
        .services(process.env.TWILIO_SERVICE_SID)
        .verifications.create({ to: numero, channel: "sms" });

      return res.send("Successful resubmission");
    } catch (err) {
      return res.status(400).send({ error: "Error sending code" });
    }
  }
}

module.exports = new GenerateCode();
