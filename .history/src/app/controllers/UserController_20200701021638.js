const UserSchema = require("../models/User");
const UserValidator = require("../validators/UserValidator");
const InvestorSchema = require("../models/Investor");
const AdvisorSchema = require("../models/Advisor");
const sgMail = require('@sendgrid/mail');
const twilio = require("twilio");

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const client = twilio(
  process.env.TWILIO_ACCOUNT_SID,
  process.env.TWILIO_AUTH_TOKEN
);

function sendEmailVerify(email){
  const msg = {
    to: 'test@example.com',
    from: 'noreply@investexpert.co',
    subject: 'Verify your e-mail',
    text: 'and easy to do anywhere, even with Node.js',
    html: '<strong>and easy to do anywhere, even with Node.js</strong>',
  };
  sgMail.send(msg);
}

class UserController {

  async store(req, res) {
    UserValidator.validate(req.body).catch(function (err) {
      return res.status(400).send({ error: `${err.name}: ${err.errors}` });
    });

    const { email, mobile, userType } = req.body;

    try {
      if (await UserSchema.findOne({ email })) {
        return res.status(400).send({ error: "E-mail is already registered." });
      }
      if (await UserSchema.findOne({ mobile })) {
        return res
          .status(400)
          .send({ error: "Mobile number is already registered." });
      }

      const obj = {
        ...req.body,
      };

      const user = await UserSchema.create(obj);

      console.log("User registered successfully");

      if (userType === "investor") {
        const investor = await InvestorSchema.create({ user: user._id });
        console.log("Investor registered successfully");
      } else if (userType === "advisor") {
        const advisor = await AdvisorSchema.create({ user: user._id });
        console.log("Advisor registered successfully");
      }

      const numero = mobile;
      //   Envio do sms
      client.verify
         .services(process.env.TWILIO_SERVICE_SID)
         .verifications.create({ to: numero, channel: "sms" })
         .then((verification) => console.log(verification.status)).catch(err => console.log("ERRO", err))

      sendEmailVerify(email);
      return res.json({ user: user, usertype: advisor });
    } catch (err) {
      console.log(err);
      return res
        .status(400)
        .send({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async update(req, res) {
    try {
      const { location: url } = req.file;
      const { firstName, lastName, email, mobile } = req.body;

      const obj = {
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobile: mobile,
        url_image: url,
        confirmedEmail: email,
      };

      const response = await UserSchema.findOneAndUpdate(
        { _id: req.userId },
        obj,
        {
          new: true,
        }
      );

      return res.json({ message: "Update successfully", update: response });
    } catch (error) {
      return res
        .status(400)
        .json({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async index(req, res) {}
}

module.exports = new UserController();
