const UserSchema = require("../models/User");
const UserValidator = require("../validators/UserValidator");
const InvestorSchema = require("../models/Investor");
const AdvisorSchema = require("../models/Advisor");
const VerificationToken = require("../models/VerificationToken");
const sgMail = require("@sendgrid/mail");
const twilio = require("twilio");

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const client = twilio(
  process.env.TWILIO_ACCOUNT_SID,
  process.env.TWILIO_AUTH_TOKEN
);

class UserController {
  async store(req, res) {
    //{abortEarly: false} faz com que o erro so retorne quando roda todas validações
    UserValidator.validate(req.body, { abortEarly: false }).catch(function (
      err
    ) {
      return res.status(400).send({ error: err.errors });
    });

    const { email, mobile, userType } = req.body;

    try {
      if (await UserSchema.findOne({ email })) {
        return res.status(400).send({ error: "E-mail is already registered." });
      }
      if (await UserSchema.findOne({ mobile })) {
        return res
          .status(400)
          .send({ error: "Mobile number is already registered." });
      }

      const obj = {
        ...req.body,
      };

      const user = await UserSchema.create(obj);
      console.log("User registered successfully");

      let userTypeDetails = null;

      if (userType === "investor") {
        userTypeDetails = await InvestorSchema.create({ user: user._id });
        console.log("Investor registered successfully");
      } else if (userType === "advisor") {
        userTypeDetails = await AdvisorSchema.create({ user: user._id });
        console.log("Advisor registered successfully");
      }

      //   Envio do sms
      client.verify
        .services(process.env.TWILIO_SERVICE_SID)
        .verifications.create({ to: mobile, channel: "sms" })
        .then((verification) => console.log(verification.status))
        .catch((err) => console.log("ERRO", err));

      //Envio E-mail
      const tokenNumber = Math.floor(Math.random() * 65536);
      VerificationToken.create({ userId: user._id, token: tokenNumber });

      console.log("a enviar email");
      const msg = {
        to: email,
        from: "info@investexpert.co",
        subject: "Verify Your E-mail",
        text:
          "Click on this link to verify your email:\nhttp://" +
          req.headers.host +
          "/confirmation/" +
          tokenNumber +
          "\n",
        html: `<strong>Click on this link to verify your email:\n</strong>
                http://${req.headers.host}/confirmation/${tokenNumber}\n
                You will have 12 hours to verify your e-mail before this verification tokens expire`,
      };
      console.log("a enviar email com msg " + msg);
      await sgMail.send(msg).then(
        () => {},
        (error) => {
          console.error(error);
          if (error.response) {
            console.error(error.response.body);
          }
        }
      );

      console.log("enviado");

      return res.json({ user: user, usertype: userTypeDetails });
    } catch (err) {
      console.log(err);
      return res
        .status(400)
        .send({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async update(req, res) {
    try {
      const { location: url } = req.file;
      const { firstName, lastName, email, mobile } = req.body;

      const obj = {
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobile: mobile,
        url_image: url,
        confirmedEmail: email,
      };

      const response = await UserSchema.findOneAndUpdate(
        { _id: req.userId },
        obj,
        {
          new: true,
        }
      );

      return res.json({ message: "Update successfully", update: response });
    } catch (error) {
      return res
        .status(400)
        .json({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async index(req, res) {}
  async emailConfirmation(req, res) {
    try {
      console.log(req.params);

      // Find a matching token
      const token = VerificationToken.findOne({ token: req.params.token });
      if (!token)
        return res
          .status(400)
          .send({
            type: "email-not-verified",
            msg:
              "We were unable to find a valid token. Your token may have expired.",
          });

      // If we found a token, find a matching user
      const user = User.findOne({ _id: token._userId });
      if (!user)
        return res
          .status(400)
          .send({ msg: "We were unable to find a user for this token." });
      if (user.status === "email_checked")
        return res
          .status(400)
          .send({
            type: "email-already-verified",
            msg: "This e-mail has already been verified.",
          });

      // Verify and save the user
      user.status = "email_checked";
      user.save();

      res.status(200).send("The e-mail has been verified.");
    } catch (error) {
      console.log(error);
      return res
        .status(400)
        .send({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async resendEmailConfirmation(req, res) {}
}

module.exports = new UserController();
