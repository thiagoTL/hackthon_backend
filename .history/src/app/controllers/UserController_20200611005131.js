const UserSchema = require("../models/User");
const UserValidator = require("../validators/UserValidator")


const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const authConfig = require("../../config/auth");
const twilio = require('twilio');

const cliente = twilio(
    process.env.TWILIO_ACCOUNT_SID,
    process.env.TWILIO_AUTH_TOKEN
);


function generateToken(params = {}) {
  return jwt.sign(params, authConfig.secret, {
    expiresIn: 86400,
  });
}

class UserController {
  async create(req, res) {

    ClientValidatorSchema.validate(req.body).catch(function(err) {
      return res.status(400).send({ error: `${err.name}: ${err.errors}`});
    });

    const { email, mobile } = req.body;

    try {
      if (await InvestorSchema.findOne({ email })) {
        return res.status(400).send({ error: "E-mail is already registered." });
      }
      if (await InvestorSchema.findOne({ mobile })) {
        return res.status(400).send({ error: "Mobile number is already registered." });
      }

      //const { location: urls = "" } = req.file;

      const obj = {
        ...req.body,
      //  urls,
      };

      const client = await InvestorSchema.create(obj);

      client.password = undefined;

      console.log("Investor registered successfully");

      //Envio do sms
      const numero = req.body.mobile;
          cliente.verify.services(process.env.TWILIO_SERVICE_SID)
          .verifications
          .create({to: numero, channel: 'sms'})
          .then(verification => console.log(verification.status));
      
      
          
      return res.json({ client: client });

    } catch (err) {
      console.log(err);
      return res
        .status(400)
        .send({ error: "An error has ocurred. Please, try again. :(" });
    }
  },
};

module.exports = new UserController();