const UserSchema = require("../models/User");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const authConfig = require("../../config/auth");
const sgMail = require('@sendgrid/mail');

function generateToken(params = {}) {
  return jwt.sign(params, authConfig.secret, {
    expiresIn: 86400,
  });
}

class SessionUserController {
  async store(req, res) {
    const { email, password } = req.body;

    

    const user = await UserSchema.findOne({ email }).select("+password");

    if (!user) {
      return res.status(400).send({ error: "User not found :( " });
    }

    if (!(await bcrypt.compare(password, user.password))) {
      return res.status(400).send({ error: "Incorrect password" });
    }

    //TODO já verificou codigo SMS?

     // using Twilio SendGrid's v3 Node.js Library
// https://github.com/sendgrid/sendgrid-nodejs
console.log("a enviar email")
sgMail.setApiKey(process.env.SENDGRID_API_KEY);
const msg = {
  to: 'clahzita@gmail.com',
  from: 'clahzita@gmail.com',
  subject: 'Sending with Twilio SendGrid is Fun',
  text: 'and easy to do anywhere, even with Node.js',
  html: '<strong>and easy to do anywhere, even with Node.js</strong>',
};
console.log("a enviar email com msg "+ msg)
sgMail.send(msg);
console.log("enviado "+ msg.to)

   

    res.send({
      user: user,
      token: generateToken({ id: user.id }),
    });
  }
}

module.exports = new SessionUserController();
