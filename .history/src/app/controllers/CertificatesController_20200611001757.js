const AdvisorSchema = require("../models/Advisor");

module.exports = {
	async update(req, res){
		try{
			const profissional = await AdvisorSchema.findOneAndUpdate(
					{ _id: req.userId }, 
					req.body,
					{ new: true }
			);
			return res.send('ok');
		}
		catch(err){
			return res.status(400).send({ error: 'algo deu errado' });
		}
 	}
}