const NewProfessionalSchema = require("../models/NewProfessional");

module.exports = {
	async update(req, res){
		try{
			const profissional = await NewProfessionalSchema.findOneAndUpdate(
					{ _id: req.userId }, 
					req.body,
					{ new: true }
			);
			return res.send('ok');
		}
		catch(err){
			return res.status(400).send({ error: 'algo deu errado' });
		}
 	}
}