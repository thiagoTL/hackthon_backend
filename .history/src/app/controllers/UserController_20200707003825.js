const UserSchema = require("../models/User");
const UserValidator = require("../validators/UserValidator");
const InvestorSchema = require("../models/Investor");
const AdvisorSchema = require("../models/Advisor");
const sgMail = require('@sendgrid/mail');
const twilio = require("twilio");

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const client = twilio(
  process.env.TWILIO_ACCOUNT_SID,
  process.env.TWILIO_AUTH_TOKEN
);

class UserController {

  async store(req, res) {
    UserValidator.validate(req.body).catch(function (err) {
      return res.status(400).send({ error: `${err.name}: ${err.errors}` });
    });

    const { email, mobile, userType } = req.body;

    try {
      if (await UserSchema.findOne({ email })) {
        return res.status(400).send({ error: "E-mail is already registered." });
      }
      if (await UserSchema.findOne({ mobile })) {
        return res
          .status(400)
          .send({ error: "Mobile number is already registered." });
      }

      const obj = {
        ...req.body,
      };

      const user = await UserSchema.create(obj);
      console.log("User registered successfully");

      let userTypeDetails = null;

      if (userType === "investor") {
        userTypeDetails = await InvestorSchema.create({ user: user._id });
        console.log("Investor registered successfully");
      } else if (userType === "advisor") {
        userTypeDetails = await AdvisorSchema.create({ user: user._id });
        console.log("Advisor registered successfully");
      }

      //   Envio do sms
      client.verify
         .services(process.env.TWILIO_SERVICE_SID)
         .verifications.create({ to: mobile, channel: "sms" })
         .then((verification) => console.log(verification.status)).catch(err => console.log("ERRO", err))
      
      //Envio E-mail
           // using Twilio SendGrid's v3 Node.js Library
// https://github.com/sendgrid/sendgrid-nodejs
console.log("a enviar email")
const msg = {
  to: email,
  from: 'info@investexpert.co',
  subject: 'Verify Your E-mail',
  text: 'Click on this link to verify your email',
  html: '<strong>and easy to do anywhere, even with Node.js</strong>',
};
console.log("a enviar email com msg "+ msg)
await sgMail.send(msg);
console.log("enviado "+ msg.from)

      return res.json({ user: user, usertype: userTypeDetails });
    } catch (err) {
      console.log(err);
      return res
        .status(400)
        .send({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async update(req, res) {
    try {
      const { location: url } = req.file;
      const { firstName, lastName, email, mobile } = req.body;

      const obj = {
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobile: mobile,
        url_image: url,
        confirmedEmail: email,
      };

      const response = await UserSchema.findOneAndUpdate(
        { _id: req.userId },
        obj,
        {
          new: true,
        }
      );

      return res.json({ message: "Update successfully", update: response });
    } catch (error) {
      return res
        .status(400)
        .json({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async index(req, res) {}
}

module.exports = new UserController();
