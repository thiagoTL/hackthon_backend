const InvestorSchema = require("../models/Investor");
const UserSchema = require("../models/User");

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const authConfig = require("../../config/auth");
const twilio = require('twilio');

const cliente = twilio(
    process.env.TWILIO_ACCOUNT_SID,
    process.env.TWILIO_AUTH_TOKEN
);

class InvestorController {
   async show(req, res) {
    const id = req.params.id;

    try {
      const investor = await InvestorSchema.find({ _id: id }).exec();

      if(!investor) {
        //TODO lançar erro
      }

      const userId = investor.user;
      console.log(investor)
      console.log(userId)
      const user = await UserSchema.find({ _id: investor.user });
      
      if(!user) {
        //TODO lançar erro
      }
      
      return res.send({ investor, user });
      
      
    } catch (err) {
      return res
        .status(400)
        .send({ error: "Investor not found" + err.message });
    }
  }

  async index(req, res) {
    try {
      const investors = await InvestorSchema.find();

      if (investors.length == 0) {
        res.send("No registered investors.");
      }
      return res.send({ data: investors });
    } catch (err) {
      return res.status(400).send({ error: "An error has ocurred. Please, try again. :( " });
    }
  }

  async remove(req, res){
    const { _id } = req.body;
    try{
        const client = await InvestorSchema.findOneAndRemove({ _id });
        if(client != null){
          return res.send("Investor removed successfully.");
        }else{
          return res.send("No investor found with the id: " + _id)
        }
        
    }catch(err){
        return res.status(400).send({ error: 'An error has ocurred. Please, try again. :( '});
    }
}

async update(req, res){
    const { _id } = req.body;

    try{
        const client = await InvestorSchema.findByIdAndUpdate({_id}, req.body, { new: true });
        if(client != null){
          return res.send('Investor updated successfully.');
        }else{
          return res.send("No investor found with the id: " + _id)
        }
    }catch(err){
        return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + err.message});
    } 
}
};


module.exports = new InvestorController();