const ClientSchema = require("../models/Client");
const ClientValidatorSchema = require("../validators/ClientValidator");

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const authConfig = require("../../config/auth");

function generateToken(params = {}) {
  return jwt.sign(params, authConfig.secret, {
    expiresIn: 86400,
  });
}

module.exports = {
  async create(req, res) {

    ClientValidatorSchema.validate(req.body).catch(function(err) {
      return res.status(400).send({ error: `${err.name}: ${err.errors}`});
    });

    const { email, mobile } = req.body;

    try {
      if (await ClientSchema.findOne({ email })) {
        return res.status(400).send({ error: "E-mail is already registered." });
      }
      if (await ClientSchema.findOne({ mobile })) {
        return res.status(400).send({ error: "Telefone is already registered." });
      }

      const { location: urls = "" } = req.file;

      const obj = {
        ...req.body,
        urls,
      };

      const client = await ClientSchema.create(obj);

      client.password = undefined;

      console.log("Investor registered successfully");

      return res.json({
        client: client,
        token: generateToken({ id: client.id }),
      });
    } catch (err) {
      console.log(err);
      return res
        .status(400)
        .send({ error: "An error has ocurred. Please, try again. :(" });
    }
  },

  async show(req, res) {
    const id = req.params.id;

    try {
      const data = await ClientSchema.find({ _id: id }).exec();
      return res.send({ data });
    } catch (err) {
      return res
        .status(400)
        .send({ error: "Investor not found" + err.message });
    }
  },
  async index(req, res) {
    try {
      const data = await ClientSchema.find();

      if (data.length == 0) {
        res.send("No registered investors.");
      }
      return res.send({ data });
    } catch (err) {
      return res.status(400).send({ error: "An error has ocurred. Please, try again. :( " });
    }
  },
  async remove(req, res){
    const { _id } = req.body;
    try{
        const client = await ClientSchema.findOneAndRemove({ _id });
        if(client != null){
          return res.send("Investor was removed successfully.");
        }else{
          return res.send("Nenhum cliente encontrado com id " + _id)
        }
        
    }catch(err){
        return res.status(400).send({ error: 'An error has ocurred. Please, try again. :( '});
    }
},

async update(req, res){
    const { _id } = req.body;

    ClientValidatorSchema.validate(req.body).catch(function(err) {
      return res.status(400).send({ error: `${err.name}: ${err.errors}`});
    });

    try{
        const client = await ClientSchema.findByIdAndUpdate({_id}, req.body, { new: true });
        if(client != null){
          return res.send('Cadastro de cliente alterado com sucesso!');
        }else{
          return res.send("Nenhum cliente encontrado com id " + _id)
        }
    }catch(err){
        return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + err.message});
    } 
},
};
