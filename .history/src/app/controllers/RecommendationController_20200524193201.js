const ClientEvaluationSchema = require('../models/ClientEvaluation');
const ProfessionalSchema = require('../models/NewProfessional');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const authConfig = require('../../config/auth');

function addGrade(avaliacao, stars) {
    const sum = avaliacao.sum + stars
    const qtde = avaliacao.quantity + 1

    const final = sum / qtde

    return {
        avaliacao: {
            sum: sum,
            quantity: qtde,
            rating: final,
        }
    }
}

function removeGrade(avaliacao, stars) {
    const sum = avaliacao.sum - stars
    const qtde = avaliacao.quantity - 1

    const final = sum / qtde

    return {
        avaliacao: {
            sum: sum,
            quantity: qtde,
            rating: final,
        }
    }
}

function updateGrade(avaliacao, oldStars, newStars) {
    const sum = avaliacao.sum - oldStars + newStars
    const qtde = avaliacao.quantity

    const final = sum / qtde

    return {
        avaliacao: {
            sum: sum,
            quantity: qtde,
            rating: final,
        }
    }
}

module.exports = {
    async store(req, res) {
        const client_id = req.userId

        const professional_id = req.params

        const { content, stars } = req.body

        newEvaluation = {
            client: client_id,
            professional: professional_id.id,
            content,
            stars,
        }

        console.log(newEvaluation);
        var evaluation;

        try {
            evaluation = await ClientEvaluationSchema.create(newEvaluation);
            console.log(evaluation);

        } catch (error) {
            return res.status(400).send({ error: 'Algo deu errado ao criar avaliação :(' + error.message });
        }

        var professional;
        if (evaluation != null) {
            try {
                professional = await ProfessionalSchema.findById({_id: professional_id.id});
                console.log(professional)
            } catch (error) {
                return res.status(400).send({ error: 'Algo deu errado na busca do profissional :(' + error.message });
            }

            if (professional != null) {

                const newAvaliacao = addGrade(professional.avaliacao, stars);

                ProfessionalSchema.findByIdAndUpdate({_id: professional_id.id}, newAvaliacao, function (err, result) {
                    if (err) return res.status(400).send({ error: 'Algo deu errado na busca do profissional :(' });
                    console.log("1 document(s) updated");
                    
                });

               res.send("Avaliação criada com sucesso e atualizado avaliação do profissional. ")

            }else{
                return res.send('Algo deu errado na busca do profissional, retornou null :(');
            }
        }else{
            return res.send('Algo deu errado na busca da avaliação, retornou null :(');
        }
    },

    async update(req, res) {

        const { _id, content, stars } = req.body

        console.log(_id);

        newValues = {
            content,
            stars,
        }

        console.log(newValues);
        var evaluation;

        try {
            evaluation = await ClientEvaluationSchema.findByIdAndUpdate(_id, newValues);
            console.log(evaluation);

        } catch (error) {
            return res.status(400).send({ error: 'Algo deu errado na atualização da avaliação :(' + error.message });
        }

        var professional;
        if (evaluation != null) {
            try {
                professional = await ProfessionalSchema.findById(evaluation.professional);
                console.log(professional)
            } catch (error) {
                return res.status(400).send({ error: 'Algo deu errado na busca do profissional :(' + error.message });
            }

            if (professional != null) {

                console.log(professional.avaliacao);

                const newAvaliacao = updateGrade(professional.avaliacao, evaluation.stars, stars);

                ProfessionalSchema.findByIdAndUpdate(evaluation.professional, newAvaliacao, function (err, result) {
                    if (err) return res.status(400).send({ error: 'Algo deu errado na busca e atualização do profissional :(' });
                    console.log("1 document(s) updated");
                    
                });

               res.send("Avaliação atualizada com SUCESSO e atualizado avaliação do profissional. ")

            }else{
                return res.send('Profissional retornou null da busca :(');
            }
        }else{
            return res.send('Avaliação retornou null da busca  :(');
        }
    },
    async remove(req, res) {

        const { _id } = req.body

        console.log(_id);

        var evaluation;

        try {
            evaluation = await ClientEvaluationSchema.findByIdAndRemove(_id);
            console.log(evaluation);

        } catch (error) {
            return res.status(400).send({ error: 'Algo deu errado na remoção da avaliação :(' + error.message });
        }

        var professional;
        if (evaluation != null) {
            try {
                professional = await ProfessionalSchema.findById(evaluation.professional);
                console.log(professional)
            } catch (error) {
                return res.status(400).send({ error: 'Algo deu errado na busca do profissional :(' + error.message });
            }

            if (professional != null) {

                console.log(professional.avaliacao);

                const newAvaliacao = removeGrade(professional.avaliacao, evaluation.stars);

                ProfessionalSchema.findByIdAndUpdate(evaluation.professional, newAvaliacao, function (err, result) {
                    if (err) return res.status(400).send({ error: 'Algo deu errado na busca e atualização do profissional :(' });
                    console.log(result);
                });

               res.send("Avaliação removida com SUCESSO e atualizado avaliação do profissional. ")

            }else{
                return res.send('Profissional retornou null da busca :(');
            }
        }else{
            return res.send('Avaliação retornou null da busca  :(');
        }
    },
    async index(req, res) {

        ClientEvaluationSchema.find({}, function (err, evaluations) {
            if (err) return res.status(400).send({ error: 'Ocorreu algum erro ' + err.message });

            if (evaluations.length == 0) {
                res.send("Nenhuma avaliação cadastrada.");
            }
            return res.send({ evaluations });

        })
    },

    async show(req, res) {

        const tags = req.body;
        try{
            const data = await ClientEvaluationSchema.find(tags).populate('user');
            return res.send({ data });
        }catch(err){
            return res.status(400).send({ error: 'Algo deu errado :(' + err.message});
        }
    },

}