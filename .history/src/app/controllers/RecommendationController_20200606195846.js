const RecommendationSchema = require('../models/Recommendation');
const ProfessionalSchema = require('../models/NewProfessional');
const RecommCreateValidator = require("../validators/RecommValidator");
const RecommUpdateValidator = require("../validators/RecommValidator");


function addGradeCalc(avaliacao, stars) {
    const sum = avaliacao.sum + stars
    const qtde = avaliacao.quantity + 1

    const final = sum !=0 ? (sum / qtde) : 0;

    return {
        avaliacao: {
            sum: sum,
            quantity: qtde,
            rating: final,
        }
    }
}

function removeGradeCalc(avaliacao, stars) {
    const sum = avaliacao.sum - stars
    const qtde = avaliacao.quantity - 1

    const final = sum !=0 ? (sum / qtde) : 0;
    
    return {
        avaliacao: {
            sum: sum,
            quantity: qtde,
            rating: final,
        }
    }
}

function updateGradeCalc(avaliacao, oldStars, newStars) {
    const sum = avaliacao.sum - oldStars + newStars
    const qtde = avaliacao.quantity

    const final = sum !=0 ? (sum / qtde) : 0;

    return {
        avaliacao: {
            sum: sum,
            quantity: qtde,
            rating: final,
        }
    }
}

module.exports = {
    async store(req, res) {
        console.log('create recommendation');
        const client_id = req.userId

        console.log('client_id')
        console.log(client_id)

        const { professional_id, content, stars, client_picture } = req.body

        console.log('body')
        console.log(req.body)

        newEvaluation = {
            client: client_id,
            professional: professional_id,
            content: content,
            stars: stars,
            client_picture: client_picture
        }
        console.log('newEvaluation')
        console.log(newEvaluation);
        try {

            await RecommCreateValidator.validate(newEvaluation);
            const evaluation = await RecommendationSchema.create(newEvaluation);
            console.log('newEvaluation')
            console.log(evaluation);
            if (evaluation != null) {
                const professional = await ProfessionalSchema.findOne({ _id: professional_id });
                console.log('professional.avaliacao')
                console.log(professional.avaliacao);

                if (professional != null) {
                    const newAvaliacao = await addGradeCalc(professional.avaliacao, stars);
                    console.log('nova avalaicao do profissional')
                    console.log(newAvaliacao);

                    const professionalUpdated = await ProfessionalSchema.findByIdAndUpdate({ _id: professional_id }, newAvaliacao, { new: true })
                    console.log("The professional rating was updated");
                    console.log(professionalUpdated.avaliacao);
                    res.send({
                        evaluation,
                        professionalUpdated,
                    })
                }
            }
        } catch (err) {
            return res.json({ error: `${err.name}: ${err.message}` });
        }
    },

    async update(req, res) {

        const { _id, client_picture, content, stars } = req.body

        console.log(_id)

        const newValues = {
            client_picture: client_picture,
            content: content,
            stars: stars,
        }
        console.log(newValues);

        try {
            await RecommUpdateValidator.validate(newValues);
            const evaluation = await RecommendationSchema.findByIdAndUpdate(_id, newValues);
            console.log("The evaluation was updated");
            console.log(evaluation);
            if (evaluation != null) {
                const professional = await ProfessionalSchema.findOne(evaluation.professional);

                if (professional != null) {
                    console.log(professional.avaliacao);

                    const newAvaliacao = await updateGradeCalc(professional.avaliacao, evaluation.stars, stars);
                    console.log('newAvaliacao')
                    console.log(newAvaliacao)
                    const professionalUpdated = await ProfessionalSchema.findByIdAndUpdate(professional, newAvaliacao, {new:true});

                    console.log("The professional rating was updated");
                    res.send({
                        professionalUpdated,
                    })
                }
            }
            console.log("fim do try");
        } catch (err) {
            return res.json({ error: `${err.name}: ${err.message}` });
        }

    },
    async remove(req, res) {

        const _id = req.body

        console.log(_id);

        try {
            const evaluation = await RecommendationSchema.findOne(_id);
            console.log(evaluation);
            await RecommendationSchema.remove(evaluation);
            if (evaluation != null) {
                const professional = await ProfessionalSchema.findOne(evaluation.professional);
                console.log(professional)

                if (professional != null) {
                    console.log(professional.avaliacao);

                    const newAvaliacao = removeGradeCalc(professional.avaliacao, evaluation.stars);
                    console.log('newAvaliacao')
                    console.log(newAvaliacao)
                    const professionalUpdated = await ProfessionalSchema.findByIdAndUpdate(evaluation.professional, newAvaliacao, {new:true});

                    console.log("Recommendation remove and professional rating update was performed successfully.")
                    res.send({
                        professionalUpdated,
                    })



                }
            }

        } catch (error) {
            return res.status(400).send({ error: 'An error has ocurred. Please, try again. :( =>' + error.message });
        }

    },
    async index(req, res) {

        try {
            const data = await RecommendationSchema.find().populate('user');
            return res.send({ data });
        } catch (err) {
            return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + err.message });
        }
    },

    async show(req, res) {

        const tags = req.params;
        try {
            const data = await RecommendationSchema.find(tags).populate('user');
            return res.send({ data });
        } catch (err) {
            return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + err.message });
        }
    },

    async removeAllProfessionalEvaluation(req, res) {

        const professional = req.body;

        const cleanEvaluation = { avaliacao: { sum: 0, quantity: 0, rating: 0 } }

        console.log({ professional: professional._id });


        try {
            const professionalUpdated = await ProfessionalSchema.findOneAndUpdate(professional, cleanEvaluation);

            await RecommendationSchema.deleteMany({ professional: professional._id });

            return res.send(professionalUpdated);
        } catch (err) {
            return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + err.message });
        }
    },

}