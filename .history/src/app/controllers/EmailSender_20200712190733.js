const UserSchema = require("../models/User");
const UserValidator = require("../validators/UserValidator");
const InvestorSchema = require("../models/Investor");
const AdvisorSchema = require("../models/Advisor");
const VerificationToken = require("../models/VerificationToken");
const sgMail = require("@sendgrid/mail");
const twilio = require("twilio");

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

class UserController {
  async emailConfirmation(req, res) {
    try {
      console.log(req.params);

      // Find a matching token
      const token = VerificationToken.findOne({ token: req.params.token });
      if (!token)
        return res.status(400).send({
          type: "email-not-verified",
          msg:
            "We were unable to find a valid token. Your token may have expired.",
        });

      // If we found a token, find a matching user
      const user = UserSchema.findOne({ _id: token._userId });
      if (!user)
        return res
          .status(400)
          .send({ msg: "We were unable to find a user for this token." });
      if (user.status === "email_checked")
        return res.status(400).send({
          type: "email-already-verified",
          msg: "This e-mail has already been verified.",
        });

      // Verify and save the user
      const mobile = user.mobile;
      const userUpdated = await UserSchema.findOneAndUpdate(
        { mobile },
        { checkedEmail: true },
        { new: true }
      );

      res.status(200).send("The e-mail has been verified.");
    } catch (error) {
      console.log(error);
      return res
        .status(400)
        .send({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async resendEmailToken(req, res) {
    try {
      req.validate;
    } catch (error) {}
  }
}
