const UserSchema = require("../models/User");
const UserValidator = require("../validators/UserValidator");
const InvestorSchema = require("../models/Investor");
const AdvisorSchema = require("../models/Advisor");

const twilio = require('twilio');

const investor = twilio(
    process.env.TWILIO_ACCOUNT_SID,
    process.env.TWILIO_AUTH_TOKEN
);

class UserController {
    async store(req, res) {

        UserValidator.validate(req.body).catch(function (err) {
            return res.status(400).send({ error: `${err.name}: ${err.errors}` });
        });

        const { email, mobile, userType } = req.body;

        try {
            if (await UserSchema.findOne({ email })) {
                return res.status(400).send({ error: "E-mail is already registered." });
            }
            if (await UserSchema.findOne({ mobile })) {
                return res.status(400).send({ error: "Mobile number is already registered." });
            }

            const obj = {
                ...req.body,
            };

            const user = await UserSchema.create(obj);

            user.password = undefined; //Não sei o pq disso

            console.log("User registered successfully");

            if (userType === "investor") {
                const investor = await InvestorSchema.create({ user: user._id });
                console.log("Investor registered successfully");
                return res.json({ user: user, investor: investor });
            } else if (userType === "advisor") {
                const advisor = await AdvisorSchema.create({ user: user._id });
                console.log("Advisor registered successfully");
                return res.json({ user: user, advisor: advisor });
            } else {
                //TODO Criar usuário admin?
                return res.json({ user: user });
            } 

            //Envio do sms
            const numero = req.body.mobile;
            investor.verify.services(process.env.TWILIO_SERVICE_SID)
                .verifications
                .create({ to: numero, channel: 'sms' })
                .then(verification => console.log(verification.status));      
            
        } catch (err) {
            console.log(err);
            return res
                .status(400)
                .send({ error: "An error has ocurred. Please, try again. :(" });
        }
    }

    async index(req, res) {
        
    }
};

module.exports = new UserController();