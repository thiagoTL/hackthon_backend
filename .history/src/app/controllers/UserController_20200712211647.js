const UserSchema = require("../models/User");
const UserValidator = require("../validators/UserValidator");
const InvestorSchema = require("../models/Investor");
const AdvisorSchema = require("../models/Advisor");
const EmailSender =  require("./EmailSender");

const twilio = require("twilio");


const client = twilio(
  process.env.TWILIO_ACCOUNT_SID,
  process.env.TWILIO_AUTH_TOKEN
);

class UserController {
  async store(req, res) {
    try {
      //{abortEarly: false} faz com todas as validações rode e retorne todos erros encontrados
      await UserValidator.validate(req.body).catch(function (err) {
        return res.status(400).json({ error: err.errors });
      });

      const { email, mobile, userType } = req.body;

      if (await UserSchema.findOne({ email })) {
        return res.status(401).json({ error: "E-mail is already registered." });
      }
      if (await UserSchema.findOne({ mobile })) {
        return res
          .status(400)
          .json({ error: "Mobile number is already registered." });
      }

      const obj = {
        ...req.body,
      };

      const user = await UserSchema.create(obj);
      console.log("User registered successfully");

      let userTypeDetails = null;

      if (userType === "investor") {
        userTypeDetails = await InvestorSchema.create({ user: user._id });
        console.log("Investor registered successfully");
      } else if (userType === "advisor") {
        userTypeDetails = await AdvisorSchema.create({ user: user._id });
        console.log("Advisor registered successfully");
      }

      //   Envio do sms
      client.verify
        .services(process.env.TWILIO_SERVICE_SID)
        .verifications.create({ to: mobile, channel: "sms" })
        .then((verification) => console.log(verification.status))
        .catch((err) => console.log("ERRO", err));

      //Envio E-mail
      EmailSender.resendEmailToken({email: email})
      const tokenNumber = Math.floor(Math.random() * 65536);
      VerificationToken.create({ userId: user._id, token: tokenNumber });

      console.log("a enviar email");
      const msg = {
        to: email,
        from: "info@investexpert.co",
        subject: "Verify Your E-mail",
        text:
          "Click on this link to verify your email:\nhttp://" +
          req.headers.host +
          "/confirmation/" +
          tokenNumber +
          "\n",
        html: `<strong>Click on this link to verify your email:\n</strong>
                http://${req.headers.host}/confirmation/${tokenNumber}\n
                \nYou will have 12 hours to verify your e-mail before this verification tokens expire.`,
      };
      console.log("a enviar email com msg " + msg);
      await sgMail.send(msg).then(
        () => {},
        (error) => {
          console.error(error);
          if (error.response) {
            console.error(error.response.body);
          }
        }
      );

      console.log("enviado");

      return res.json({ user: user, usertype: userTypeDetails });
    } catch (err) {
      console.log(err);
      return res
        .status(400)
        .send({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async update(req, res) {
    try {
      const { location: url } = req.file;
      const { firstName, lastName, email, mobile } = req.body;

      const obj = {
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobile: mobile,
        url_image: url,
        confirmedEmail: email,
      };

      const response = await UserSchema.findOneAndUpdate(
        { _id: req.userId },
        obj,
        {
          new: true,
        }
      );

      return res.json({ message: "Update successfully", update: response });
    } catch (error) {
      return res
        .status(400)
        .json({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async index(req, res) {}
}

module.exports = new UserController();
