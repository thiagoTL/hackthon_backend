const RecommendationSchema = require('../models/Recommendation');
const AdvisorSchema = require('../models/Advisor');
const RecommCreateValidator = require("../validators/RecommValidator");
const RecommUpdateValidator = require("../validators/RecommValidator");


function addGradeCalc(avaliacao, stars) {
    const sum = avaliacao.sum + stars
    const qtde = avaliacao.quantity + 1

    const final = sum != 0 ? (sum / qtde) : 0;

    return {
        avaliacao: {
            sum: sum,
            quantity: qtde,
            rating: final,
        }
    }
}

function removeGradeCalc(avaliacao, stars) {
    const sum = avaliacao.sum - stars
    const qtde = avaliacao.quantity - 1

    const final = sum != 0 ? (sum / qtde) : 0;

    return {
        avaliacao: {
            sum: sum,
            quantity: qtde,
            rating: final,
        }
    }
}

function updateGradeCalc(avaliacao, oldStars, newStars) {
    const sum = avaliacao.sum - oldStars + newStars
    const qtde = avaliacao.quantity

    const final = sum != 0 ? (sum / qtde) : 0;

    return {
        avaliacao: {
            sum: sum,
            quantity: qtde,
            rating: final,
        }
    }
}

class RecommendationController {
    async store(req, res) {
        console.log('creating recommendation');
        const client_id = req.userId

        console.log('client_id')
        console.log(client_id)

        const { professional_id, content, stars, client_picture } = req.body

        try {


            console.log('profissional')
            console.log(professional_id)

            console.log('body')
            console.log(req.body)

            const newEvaluation = {
                client: client_id,
                professional: professional_id,
                content: content,
                stars: stars,
                client_picture: client_picture
            }

            console.log('newEvaluation')
            console.log(newEvaluation);

            await AdvisorSchema.findOne({ _id: professional_id }, (err, result) => {
                if (!result) {
                    return res.status(400).send({ error: "Professional Id is not valid" });
                }
            });

            await RecommendationSchema.findOne({ client: client_id, professional: professional_id }, (err, result) => {
                if (result) {
                    return res.status(400).send({ error: "Recommendation is already registered." });
                }
            });


            await RecommCreateValidator.validate(newEvaluation);

            const evaluation = await RecommendationSchema.create(newEvaluation);
            console.log('newEvaluation')
            console.log(evaluation);

            if (evaluation != null) {
                const professional = await AdvisorSchema.findOne({ _id: professional_id });
                console.log('professional.avaliacao')
                console.log(professional.avaliacao);

                if (professional != null) {
                    const newAvaliacao = addGradeCalc(professional.avaliacao, stars);
                    console.log('nova avalaicao do profissional')
                    console.log(newAvaliacao);

                    const professionalUpdated = await AdvisorSchema.findByIdAndUpdate({ _id: professional_id }, newAvaliacao, { new: true })
                    console.log("The professional rating was updated");
                    console.log(professionalUpdated.avaliacao);
                    res.send({
                        evaluation,
                        professionalUpdated,
                    })
                }
            }
        } catch (err) {
            console.log(err);
            return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(' });
        }
    }

    async update(req, res) {

        const { _id, client_picture, content, stars } = req.body

        console.log(_id)

        await RecommendationSchema.findOne({ _id: _id }, (err, recomm) => {
            if (!recomm) {
                return res.status(400).send({ error: "Recommendation Id is not valid" });
            }
            if (recomm.client != req.userId) {
                return res.status(400).send({ error: "Client doesn't have authorization to change this recommendation." });
            }
        });

        const newValues = {
            client_picture: client_picture,
            content: content,
            stars: stars,
        }
        console.log(newValues);

        try {
            await RecommUpdateValidator.validate(newValues);
            const evaluation = await RecommendationSchema.findByIdAndUpdate(_id, newValues, { new: true });
            console.log("The evaluation was updated");
            console.log(evaluation);
            if (evaluation != null) {
                const professional = await AdvisorSchema.findOne(evaluation.professional);

                if (professional != null) {
                    console.log(professional.avaliacao);

                    const newAvaliacao = await updateGradeCalc(professional.avaliacao, evaluation.stars, stars);
                    console.log('newAvaliacao')
                    console.log(newAvaliacao)
                    const professionalUpdated = await AdvisorSchema.findByIdAndUpdate(professional, newAvaliacao, { new: true });

                    console.log("The professional rating was updated");
                    res.send({
                        professionalUpdated,
                    })
                }
            }
            console.log("fim do try");
        } catch (err) {
            console.log(err);
            return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(' });
        }

    }

    async remove(req, res) {

        const _id = req.body

        console.log(_id);

        const recomm = await RecommendationSchema.findOne({ _id: _id });

        if (!recomm) {
            return res.status(400).send({ error: "Recommendation Id is not valid" });
        }

        if (recomm.client != req.userId) {
            return res.status(400).send({ error: "Client doesn't have authorization to remove this recommendation." });
        }

        try {
            const evaluation = recomm;
            console.log(recomm);
            await RecommendationSchema.remove(evaluation);
            if (evaluation != null) {
                const professional = await AdvisorSchema.findOne(evaluation.professional);
                console.log(professional)

                if (professional != null) {
                    console.log(professional.avaliacao);

                    const newAvaliacao = removeGradeCalc(professional.avaliacao, evaluation.stars);
                    console.log('newAvaliacao')
                    console.log(newAvaliacao)
                    const professionalUpdated = await AdvisorSchema.findByIdAndUpdate(evaluation.professional, newAvaliacao, { new: true });

                    console.log("Recommendation remove and professional rating update was performed successfully.")
                    res.send({
                        professionalUpdated,
                    })



                }
            }

        } catch (err) {
            console.log(err);
            return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(' });
        }

    }

    async index(req, res) {

        try {
            const data = await RecommendationSchema.find().populate('client');
            return res.send({ data });
        } catch (err) {
            console.log({ error: `${err.name}: ${err.message}` });
            return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(' });
        }
    }

    async show(req, res) {

        const tags = req.body;
        try {
            const recomm = await RecommendationSchema.find(tags).populate('client');
            return res.send({ recomm: recomm });
        } catch (err) {
            console.log(err);
            return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(' });
        }
    }

    async removeAllProfessionalEvaluation(req, res) {

        const professional = req.body;

        const cleanEvaluation = { avaliacao: { sum: 0, quantity: 0, rating: 0 } }

        console.log({ professional: professional._id });


        try {
            const professionalUpdated = await AdvisorSchema.findOneAndUpdate(professional, cleanEvaluation);

            await RecommendationSchema.deleteMany({ professional: professional._id });

            return res.send(professionalUpdated);
        } catch (err) {
            console.log(err);
            return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(' });
        }
    }

}

module.exports = new RecommendationController();