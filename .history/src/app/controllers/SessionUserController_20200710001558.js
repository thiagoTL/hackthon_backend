const UserSchema = require("../models/User");
const VerificationToken = require("../models/VerificationToken");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const authConfig = require("../../config/auth");

function generateToken(params = {}) {
  return jwt.sign(params, authConfig.secret, {
    expiresIn: 86400,
  });
}

class SessionUserController {
  async store(req, res) {
    const { email, password } = req.body;

    const user = await UserSchema.findOne({ email }).select("+password");

    if (!user) {
      return res.status(400).send({ error: "User not found :( " });
    }

    if (!(await bcrypt.compare(password, user.password))) {
      return res.status(400).send({ error: "Incorrect password" });
    }

    if(!user.verifiedSms){
      return res.status(401).send({ error: "Your mobile has not been verified." });
      //TODO devemos reenviar o codigo aqui?
    }

    //TODO precisamos checar se email foi verificado ou isso nao impede de fazer login?

    res.send({
      user: user,
      token: generateToken({ id: user.id }),
    });
  }
}

module.exports = new SessionUserController();
