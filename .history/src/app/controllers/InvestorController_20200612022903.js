const InvestorSchema = require("../models/Investor");
const UserSchema = require("../models/User");

class InvestorController {
   async show(req, res) {
    
    const id  = req.params.id;

    console.log(id)

    try {
      const investor = await InvestorSchema.find({ _id: id }).exec();

      if(!investor) {
        return res.send("No investor found with the id: " + id)
      }
      console.log(investor)
      const userId = investor[0].user;
      console.log(userId)
      const user = await UserSchema.find({ _id: userId });
      
      if(!user) {
        return res.send("No user found with the id: " + userId)
      }
      
      return res.send({ investor, user });
      
      
    } catch (err) {
      console.log(err)
      return res
        .status(400)
        .send({ error: "Investor not found"});
    }
  }

  async index(req, res) {
    
    try {
      var investorsAndUsers = new Array();

      const investors = await InvestorSchema.find();

      if (investors.length == 0) {
        res.send("No registered investors.");
      }
      
      investorsAndUsers.push(investors.forEach(async investor => {
        const userId = investor.user;        
        const user = await UserSchema.findOne({ _id: userId });
        const userAndInvestor = { investor, user };  
        console.log(userAndInvestor)    
        return userAndInvestor;
      }));

      console.log(investorsAndUsers)
      return res.send({investorsAndUsers});
    } catch (err) {
      console.log(err)
      return res.status(400).send({ error: "An error has ocurred. Please, try again. :( "});
    }
  }

  async remove(req, res){
    const { _id } = req.body;
    try{
        const investor = await InvestorSchema.findOneAndRemove({ _id });
        if(!investor){
          return res.send("No investor found with the id: " + _id);
        }
        const userId = investor[0].user;
        const user = await UserSchema.findOneAndRemove({ _id:userId });

        if(!user){
          return res.send("No user found with the id: " + userId);
        }
        
        return res.send("Investor removed successfully.");      
        
    }catch(err){
        return res.status(400).send({ error: 'An error has ocurred. Please, try again. :( '});
    }
}

async update(req, res){
    const { _id } = req.body;

    try{
        const client = await InvestorSchema.findByIdAndUpdate({_id}, req.body, { new: true });
        if(client != null){
          return res.send('Investor updated successfully.');
        }else{
          return res.send("No investor found with the id: " + _id)
        }
    }catch(err){
        return res.status(400).send({ error: 'An error has ocurred. Please, try again. :(\n' + err.message});
    } 
}
};


module.exports = new InvestorController();