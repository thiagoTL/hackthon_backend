const twilio = require("twilio");
const UserSchema = require("../models/User");


const client = twilio(
  process.env.TWILIO_ACCOUNT_SID,
  process.env.TWILIO_AUTH_TOKEN
);

class GenerateCode {
  async confirm(req, res) {
    try {
      const { code, mobile } = req.body;

      const user = req.userId;

      const codigo = code;
      const numero = mobile;

      client.verify
        .services(process.env.TWILIO_SERVICE_SID)
        .verificationChecks.create({ to: numero, code: codigo })
        .then((verification_check) => res.json(verification_check));
        


        await InvestorSchema.findOne({ user }, (err, result) => {
          if (!result) {
              return res.status(400).send({ error: "User is not an investor." });
          }
          client_id = result._id;
      });  

      return res.json({
        message: "Successfully approved code",
      });
    } catch (error) {
      return res.json({ message: "Error generate code" });
    }
  }

  async resend(req, res) {
    const { mobile } = req.body;
    try{
      const numero = mobile;

      client.verify
        .services(process.env.TWILIO_SERVICE_SID)
        .verifications
        .create({ to: numero, channel: 'sms' });
      
      return res.send('Successful resubmission');
    }catch(err){
      return res.status(400).send({ error: 'Error sending code' });
    }
  }
};

module.exports = new GenerateCode();
