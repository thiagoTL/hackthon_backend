const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const twilio = require("twilio");
const UserSchema = require("../models/User");
const authConfig = require("../../config/auth");

const client = twilio(
  process.env.TWILIO_ACCOUNT_SID,
  process.env.TWILIO_AUTH_TOKEN
);

function generateToken(params = {}) {
  return jwt.sign(params, authConfig.secret, {
    expiresIn: 86400,
  });
}

class GenerateCode {
  async confirm(req, res) {
    const { code, mobile } = req.body;

    // Find a matching user
    UserSchema.findOne({ mobile: mobile }, function (err, user) {
      if (!user)
        return res
          .status(400)
          .send({ msg: "We were unable to find a user for this mobile." });
      if (user.checkedSms)
        return res
          .status(400)
          .send({
            type: "mobile-already-verified",
            msg: "This user mobile has already been verified.",
          });

      client.verify
        .services(process.env.TWILIO_SERVICE_SID)
        .verificationChecks.create({ to: mobile, code: code })
        .then((verification_check) => res.json(verification_check));

      // Verify and save the user
      user.checkedSms = true;
      console.log(user);
      user.save(function (err) {
        if (err) {
          return res.status(500).send({ msg: err.message });
        }

        return res.status(200).send({
          message: "The mobile has been verified.",
          user: user,
          token: generateToken({ id: user.id }),
        });
      });
    });
  }

  async resend(req, res) {
    const { mobile } = req.body;
    try {
      const numero = mobile;

      console.log("NUMERO", mobile);

      client.verify
        .services(process.env.TWILIO_SERVICE_SID)
        .verifications.create({ to: numero, channel: "sms" })
        .then((verification_check) => console.log(verification_check));

      return res.send("Successful resubmission");
    } catch (err) {
      return res.status(400).send({ error: "Error sending code" });
    }
  }
}

module.exports = new GenerateCode();
