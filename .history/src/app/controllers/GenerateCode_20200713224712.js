const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const twilio = require("twilio");
const UserSchema = require("../models/User");
const authConfig = require("../../config/auth");

const client = twilio(
  process.env.TWILIO_ACCOUNT_SID,
  process.env.TWILIO_AUTH_TOKEN
);

function generateToken(params = {}) {
  return jwt.sign(params, authConfig.secret, {
    expiresIn: 86400,
  });
}

const yup = require("yup");

const MobileValidator = yup.object().shape({
  phone: yup.number()
    .typeError("That doesn't look like a mobile number")
    .positive("A mobile number can't start with a minus")
    .integer("A mobile number can't include a decimal point")
    .min(8)
    .required("A mobile number is required"),
});

class GenerateCode {
  async confirm(req, res) {
    const { code, mobile } = req.body;

    // Find a matching user
    UserSchema.findOne({ mobile: mobile }, function (err, user) {
      if (!user)
        return res
          .status(400)
          .send({ msg: "We were unable to find a user for this mobile." });
      if (user.checkedSms)
        return res.status(400).send({
          type: "mobile-already-verified",
          msg: "This user mobile has already been verified.",
        });

      client.verify
        .services(process.env.TWILIO_SERVICE_SID)
        .verificationChecks.create({ to: mobile, code: code })
        .then((verification_check) => res.json(verification_check));

      // Verify and save the user
      user.checkedSms = true;
      console.log(user);
      user.save(function (err) {
        if (err) {
          return res.status(500).send({ msg: err.message });
        }

        return res.status(200).send({
          message: "The mobile has been verified.",
          user: user,
          token: generateToken({ id: user.id }),
        });
      });
    });
  }

  //TODO fazer verificações!!!!
  async resend(req, res) {

    // Check for validation errors
    MobileValidator.validate(req.body).catch(function (err) {
      return res.status(400).json({ error: err.errors });
    });

    const { mobile } = req.body;

    UserSchema.findOne({ mobile: mobile }, function (err, user) {
      console.log(user);
      if (!user)
        return res
          .status(400)
          .send({ msg: "We were unable to find a user with that mobile." });
      if (user.checkedSms)
        return res
          .status(400)
          .send({ msg: "This mobile has already been verified." });

      //   Envio do sms
      client.verify
        .services(process.env.TWILIO_SERVICE_SID)
        .verifications.create({ to: mobile, channel: "sms" })
        .then((verification) => console.log(verification.status))
        .catch((err) => console.log("ERRO", err));

      return res.send("Successful resubmission");

  }
}

  //Apenas para admin testar envio de codigo
  async resendCode(req, res) {
    const { mobile } = req.body;
    try {
      const numero = mobile;

      console.log("NUMERO", mobile);

      //   Envio do sms
      client.verify
        .services(process.env.TWILIO_SERVICE_SID)
        .verifications.create({ to: mobile, channel: "sms" })
        .then((verification) => console.log(verification.status))
        .catch((err) => console.log("ERRO", err));

      return res.send("Successful resubmission");
    } catch (err) {
      return res.status(400).send({ error: "Error sending code" });
    }
  }
}

module.exports = new GenerateCode();
