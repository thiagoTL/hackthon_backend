const UserSchema = require("../models/User");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const authConfig = require("../../config/auth");

function generateToken(params = {}) {
  return jwt.sign(params, authConfig.secret, {
    expiresIn: 86400,
  });
}

class SessionUserController {
  async store(req, res) {
    const { email, password } = req.body;

    

    const user = await UserSchema.findOne({ email }).select("+password");

    if (!user) {
      return res.status(400).send({ error: "User not found :( " });
    }

    if (!(await bcrypt.compare(password, user.password))) {
      return res.status(400).send({ error: "Incorrect password" });
    }

    //TODO já verificou codigo SMS?

    res.send({
      user: user,
      token: generateToken({ id: user.id }),
    });
  }
}

module.exports = new SessionUserController();
