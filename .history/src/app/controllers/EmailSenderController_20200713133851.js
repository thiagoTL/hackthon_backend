const UserSchema = require("../models/User");
const VerificationToken = require("../models/VerificationToken");
const sgMail = require("@sendgrid/mail");

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const yup = require("yup");

const EmailValidator = yup.object().shape({
  email: yup
        .string()
        .email()
        .required()
})

class EmailSenderController {
  async emailConfirmation(req, res) {
    try {
      console.log(req.params);

      // Find a matching token
      const token = VerificationToken.findOne({ token: req.params.token });
      if (!token)
        return res.status(400).send({
          type: "email-not-verified",
          msg:
            "We were unable to find a valid token. Your token may have expired.",
        });

      // If we found a token, find a matching user
      let user = UserSchema.findOne({ _id: token._userId });
      console.log(user);
      if (!user)
        return res
          .status(400)
          .send({ msg: "We were unable to find a user for this token." });
      if (user.checkedEmail)
        return res.status(400).send({
          type: "email-already-verified",
          msg: "This e-mail has already been verified.",
        });
        
        console.log(user);
      // Verify and save the user
      user.checkedEmail = true;

      (await user).save(function (err) {
        if (err) { return res.status(500).send({ msg: err.message });}
      });
      
      console.log(user);

      res.status(200).send("The e-mail has been verified.");
    } catch (error) {
      console.log(error);
      return res
        .status(400)
        .send({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async resendEmailToken(req, res) {
 
    // Check for validation errors    
    EmailValidator.validate(req.body).catch(function (err) {
      return res.status(400).json({ error: err.errors });
    });

    UserSchema.findOne({ email: req.body.email }, function (err, user) {
        if (!user) return res.status(400).send({ msg: 'We were unable to find a user with that email.' });
        if (user.checkedEmail) return res.status(400).send({ msg: 'This e-mail has already been verified.' });
 
        // Create a verification token, save it, and send email
        //TODO Use crypto: crypto.randomBytes(16).toString('hex')
        const tokenNumber = Math.floor(Math.random() * 65536);
        var token = new VerificationToken({ userId: user._id, token: tokenNumber });
 
        // Save the token
        token.save(function (err) {
            if (err) { return res.status(500).send({ msg: err.message }); }
 
            // Send the email
            const msg = {
                to: user.email,
                from: "info@investexpert.co",
                subject: "Verify Your E-mail",
                text:
                  "Hello, \n\nClick on this link to verify your email:\n\nhttp://" +
                  req.headers.host +
                  "/confirmation/" +
                  tokenNumber +
                  "\n\n" +
                  "You will have 12 hours to verify your e-mail before this verification tokens expire.",
                html: `<strong>Click on this link to verify your email:\n\n</strong>
                        http://${req.headers.host}/confirmation/${tokenNumber}\n\n
                        You will have 12 hours to verify your e-mail before this verification tokens expire.`,
              };
              console.log("a enviar email com msg " + msg);
              sgMail.send(msg, function (err) {
                if (err) { return res.status(500).send({ msg: err.message }); }
                res.status(200).send('A verification email has been sent to ' + user.email + '.');
            });
        });
 
    });
}
}

module.exports = new EmailSenderController();
