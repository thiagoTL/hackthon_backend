const UserSchema = require("../models/User");
const UserValidator = require("../models/validators/UserValidator");
const InvestorSchema = require("../models/Investor");
const AdvisorSchema = require("../models/Advisor");

const twilio = require("twilio");


const client = twilio(
  process.env.TWILIO_ACCOUNT_SID,
  process.env.TWILIO_AUTH_TOKEN
);

class UserController {
  async store(req, res) {
    try {
      //{abortEarly: false} faz com todas as validações rode e retorne todos erros encontrados
      await UserValidator.validate(req.body).catch(function (err) {
        return res.status(400).json({ error: err.errors });
      });

      const { email, mobile, userType } = req.body;

      if (await UserSchema.findOne({ email })) {
        return res.status(401).json({ error: "E-mail is already registered." });
      }
      if (await UserSchema.findOne({ mobile })) {
        return res
          .status(400)
          .json({ error: "Mobile number is already registered." });
      }

      const obj = {
        ...req.body,
      };

      const user = await UserSchema.create(obj);
      console.log("User registered successfully");

      let userTypeDetails = null;

      if (userType === "investor") {
        userTypeDetails = await InvestorSchema.create({ user: user._id });
        console.log("Investor registered successfully");
      } else if (userType === "advisor") {
        userTypeDetails = await AdvisorSchema.create({ user: user._id });
        console.log("Advisor registered successfully");
      }

      //   Envio do sms
      client.verify
        .services(process.env.TWILIO_SERVICE_SID)
        .verifications.create({ to: mobile, channel: "sms" })
        .then((verification) => console.log(verification.status))
        .catch((err) => console.log("ERRO", err));

      //Envio E-mail
      // Create a verification token, save it, and send email
        //TODO Use crypto: crypto.randomBytes(16).toString('hex')
        const tokenNumber = Math.floor(Math.random() * 65536);
        var token = new VerificationToken({ userId: user._id, token: tokenNumber });
 
        // Save the token
        token.save(function (err) {
            if (err) { return res.status(500).send({ msg: err.message }); }
 
            // Send the email
            const msg = {
                to: user.email,
                from: "info@investexpert.co",
                subject: "Verify Your E-mail",
                text:
                  "Hello, \n\nClick on this link to verify your email:\n\nhttp://" +
                  req.headers.host +
                  "/confirmation/" +
                  tokenNumber +
                  "\n\n" +
                  "You will have 12 hours to verify your e-mail before this verification tokens expire.",
                html: `<strong>Click on this link to verify your email:\n\n</strong>
                        http://${req.headers.host}/confirmation/${tokenNumber}\n\n
                        You will have 12 hours to verify your e-mail before this verification tokens expire.`,
              };
              console.log("a enviar email com msg " + msg);
              sgMail.send(msg, function (err) {
                if (err) { return res.status(500).send({ msg: err.message }); }
                res.status(200).send({user: user, usertype: userTypeDetails, msg: 'A verification email has been sent to ' + user.email + '.'});
            });
        });
    } catch (err) {
      console.log(err);
      return res
        .status(400)
        .send({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async update(req, res) {
    try {
      const { location: url } = req.file;
      const { firstName, lastName, email, mobile } = req.body;

      const obj = {
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobile: mobile,
        url_image: url,
        confirmedEmail: email,
      };

      const response = await UserSchema.findOneAndUpdate(
        { _id: req.userId },
        obj,
        {
          new: true,
        }
      );

      return res.json({ message: "Update successfully", update: response });
    } catch (error) {
      return res
        .status(400)
        .json({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async index(req, res) {}
}

module.exports = new UserController();
