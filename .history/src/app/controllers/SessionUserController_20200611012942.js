const UserSchema = require("../models/User");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const authConfig = require("../../config/auth");

function generateToken(params = {}) {
  return jwt.sign(params, authConfig.secret, {
    expiresIn: 86400,
  });
}

class SessionClientUserController {
  async store(req, res) {
    const { email, password } = req.body;

    const client = await UserSchema.findOne({ email }).select("+password");

    if (!client) {
      return res.status(400).send({ error: "Cliente não encontrado :( " });
    }

    if (!(await bcrypt.compare(password, client.password))) {
      return res.status(400).send({ error: "Senha incorreta " });
    }

    client.password = undefined;

    res.send({
      client: client,
      token: generateToken({ id: client.id }),
    });
  }
}

module.exports = new SessionClientUserController();
