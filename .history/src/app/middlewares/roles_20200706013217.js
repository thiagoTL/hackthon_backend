const AccessControl = require("accesscontrol");

// This is actually how the grants are maintained internally.
let grantsObject = {
    admin: {
        profile: {
            'create:any': ['*', '!views'],
            'read:any': ['*'],
            'update:any': ['*', '!views'],
            'delete:any': ['*']
        }
    },
    investor: {
        video: {
            'create:own': ['*', '!rating', '!views'],
            'read:own': ['*'],
            'update:own': ['*', '!rating', '!views'],
            'delete:own': ['*']
        }
    },
    advisor: {
        user: {
            'create:any': ['*', '!views'],
            'read:any': ['*'],
            'update:any': ['*', '!views'],
            'delete:any': ['*']
        }
    },
};

const ac = new AccessControl(grantsObject);

const ac = new AccessControl();
 
exports.roles = (function() {
ac.grant("investor")
 .readOwn("profile")
 .updateOwn("profile")
 
ac.grant("advisor")
 .extend("basic")
 .readAny("profile")
 
ac.grant("admin")
 .extend("investor")
 .extend("advisor")
 .updateAny("profile")
 .deleteAny("profile")
 
return ac;
})();