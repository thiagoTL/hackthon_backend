const AccessControl = require("accesscontrol");
const ac = new AccessControl();
 
exports.roles = (function() {
ac.grant("investor")
 .readOwn("profile")
 .updateOwn("profile")
 
ac.grant("advisor")
 .extend("basic")
 .readAny("profile")
 
ac.grant("admin")
 .extend("investor")
 .extend("advisor")
 .updateAny("profile")
 .deleteAny("profile")
 
return ac;
})();