const { Schema, model } = require("mongoose");

const InvestorSchema = new Schema({
  user:  {
    type: Schema.Types.ObjectId,
    ref: 'UserSchema',
    required: true,
  },
});

InvestorSchema.pre("save", async function (next) {
  const hash = await bcrypt.hash(this.password, 10);
  this.password = hash;

  next();
});

module.exports = model("InvestorSchema", InvestorSchema);
