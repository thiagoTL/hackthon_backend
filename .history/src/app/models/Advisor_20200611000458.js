const { Schema, model } = require("mongoose");

const AdvisorSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'UserSchema',
    required: true,
  },
  aboutMe: {
    type: String,
    required: false,
  },
  yearsExperience: {
    type: Number,
    required: false,
  },
  accessor: {
    type: Boolean,
    required: false,
  },
  brokerCompany: {
    type: String,
    required: false,
  },
  nativeLanguage: {
    type: String,
    required: true,
  },
  secondariesLanguage: [{
    language: {
      type: String,
      required: false,
    }
  }],
  avaliacao: {
    sum: {
      type: Number,
      default: 0,
    },
    quantity: {
      type: Number,
      default: 0,
    },
    rating: {
      type: Number,
      default: 0,
    },
  },
  qualifications: {
    ba: {
      type: Boolean,
      required: false,
      default: false,
    },
    mba: {
      type: Boolean,
      required: false,
      default: false,
    },
    cfa: {
      type: Boolean,
      required: false,
      default: false,
    },
    rfp: {
      type: Boolean,
      required: false,
      default: false,
    },
    cfp: {
      type: Boolean,
      required: false,
      default: false,
    },
    cpa: {
      type: Boolean,
      required: false,
      default: false,
    },
    cim: {
      type: Boolean,
      required: false,
      default: false,
    }
  },
});

AdvisorSchema.virtual("url").get(function () {
  const url = process.env.URL || "http://localhost:5000";
  return `${url}/professionals/${encodeURIComponent(this.filesname)}`;
});

module.exports = model("AdvisorSchema", AdvisorSchema);
