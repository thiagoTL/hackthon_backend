const { Schema, model } = require("mongoose");
const bcrypt = require("bcryptjs");

const UserSchema = new Schema({
    userType: {
        type: String,
        enum: ["advisor", "investor", "admin"],
    },
    fisrtName: {
        type: String,
        required: true,
    },
    lastName: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
    },
    confirmedEmail: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
    },
    mobile: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        required: false,
        select: false,
    },
    confirmedPassword: {
        type: String,
        required: false,
        select: false,
    },
    dob: {
        type: Date,
        required: true,
    },
    linkedin: {
        type: String,
        required: false,
        unique: true,
    },
    country: {
        type: String,
        required: true,
    },
    agreeTerms: {
        type: Boolean,
        required: true,
    },
    verifiedSms: {
        type: Boolean,
        required: true,
    },
    verifiedIdentityDocumentation: {
        type: Boolean,
        required: false,
        default: false,
    },
});

NewClientSchema.pre("save", async function (next) {
    const hash = await bcrypt.hash(this.password, 10);
    this.password = hash;

    next();
});

module.exports = model("UserSchema", UserSchema);
