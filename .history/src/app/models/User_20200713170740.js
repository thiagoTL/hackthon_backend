const { Schema, model } = require("mongoose");
const bcrypt = require("bcryptjs");

const UserSchema = new Schema({
  userType: {
    type: String,
    enum: ["advisor", "investor", "admin"],
    required: true,
  },
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
  },
  confirmedEmail: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
  },
  mobile: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    required: false,
    select: false,
  },
  confirmedPassword: {
    type: String,
    required: false,
    select: false,
  },
  dob: {
    type: Date,
    required: true,
  },
  linkedin: {
    type: String,
    required: false,
    unique: false,
  },
  country: {
    type: String,
    required: true,
  },
  url_image: {
    type: String,
    required: false,
    default:
      "https://hackathongorila.s3.amazonaws.com/464529f1bfc4c5e1d884f7c213ebae5b.png",
  },
  agreeTerms: {
    type: Boolean,
    required: true,
  },
  checkedSms: {
    type: Boolean,
    default: false,
  },
  checkedEmail: {
    type: Boolean,
    default: false,
  },
  verifiedIdDocument: {
    type: Boolean,
    required: false,
    default: false,
  },
});

UserSchema.pre("save", async function (next) {

  if(this.password){
  const hash = await bcrypt.hash(this.password, 10);
  this.password = hash;
  }

  next();
});

module.exports = model("UserSchema", UserSchema);