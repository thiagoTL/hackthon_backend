const { Schema, model } = require("mongoose");
const bcrypt = require("bcryptjs");

const NewProfessionalSchema = new Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    validate: {
      validator: function (v) {
        return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(v);
      },
    },
  },
  phone: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    required: true,
    select: false,
  },
  linkedin: {
    type: String,
    required: false,
    unique: true,
  },
  assessor: {
    type: Boolean,
    required: false,
  },
  corretora: {
    type: String,
    required: false,
    default: "Independente"
  },
  state: {
    type: String,
    required: false,
  },
  city: {
    type: String,
    required: false,
  },
  avaliacao: {
    sum: {
      type: Number,
      default: 0,
    },
    quantity: {
      type: Number,
      default: 0,
    },
    rating: {
      type: Number,
      default: 0,
    },
  },
  urls: {
    type: String,
    required: false,
  },
  ba: {
    type: Boolean,
    required: false,
    default: false,
  },
  mba: {
    type: Boolean,
    required: false,
    default: false,
  },
  cfa: {
    type: Boolean,
    required: false,
    default: false,
  },
  rfp: {
    type: Boolean,
    required: false,
    default: false,
  },
  cfp: {
    type: Boolean,
    required: false,
    default: false,
  },
  cpa: {
    type: Boolean,
    required: false,
    default: false,
  },
  cim: {
    type: Boolean,
    required: false,
    default: false,
  },
});

NewProfessionalSchema.pre("save", async function (next) {
  const hash = await bcrypt.hash(this.password, 10);
  this.password = hash;

  next();
});

NewProfessionalSchema.virtual("url").get(function () {
  const url = process.env.URL || "http://localhost:5000";
  return `${url}/professionals/${encodeURIComponent(this.filesname)}`;
});

module.exports = model("NewProfessionalTesteSchema", NewProfessionalSchema);
