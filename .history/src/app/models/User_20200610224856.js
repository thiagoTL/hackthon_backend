const { Schema, model } = require("mongoose");
const bcrypt = require("bcryptjs");

const UserSchema = new Schema({
    userType: {
        type: String,
        enum: ['advisor', 'investor', 'admin'],
    },
    fisrt_name: {
        type: String,
        required: true,
    },
    last_name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
    },
    confirm_email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
    },
    mobile: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        required: false,
        select: false,
    },
    confirm_password: {
        type: String,
        required: false,
        select: false,
    },
    dob: {
        type: Date,
        required: true,
    },
    linkedin: {
        type: String,
        required: false,
        unique: true,
    },
    country: {
        type: String,
        required: true,
    },
});

NewClientSchema.pre("save", async function (next) {
    const hash = await bcrypt.hash(this.password, 10);
    this.password = hash;

    next();
});

module.exports = model("NewClientSchema", NewClientSchema);
