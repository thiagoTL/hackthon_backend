const { Schema, model } = require('mongoose');
const RecommValidatorSchema = require("../validators/RecommValidator");


const RecommendationSchema = new Schema(
    {
        client: {
            type: Schema.Types.ObjectId,
            ref: 'NewClientSchema',
            required: true,
        },
        professional: {
            type: Schema.Types.ObjectId,
            ref: 'NewProfessionalSchema',
            required: true,
        },
        content: {
            type: String,
            maxlength: 250,
        },
        stars: {
            type: Number,
            required: true,
            default: 0,
            min: 0,
            max: 5,
        },
        client_picture: {
            type: Boolean,
            default: false,
        },
        CreatedAt: {
            type: Date,
            default: Date.now,
        },
    });

RecommendationSchema.index({ client: 1, professional: 1 }, { unique: true });

RecommendationSchema.pre("save", async function (next) {
    RecommValidatorSchema.validate(this).catch(function (err) {
        return res.status(400).send({ error: `${err.name}: ${err.errors}` });
    });
},

    RecommendationSchema.pre("update", async function (next) {
        RecommValidatorSchema.validate(this).catch(function (err) {
            return res.status(400).send({ error: `${err.name}: ${err.errors}` });
        });
    },

        module.exports = model("ClientEvaluationSchema", RecommendationSchema); 