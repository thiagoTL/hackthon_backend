const { Schema, model } = require("mongoose");

const VerificationToken = new Schema({
  userId:  {
    type: Schema.Types.ObjectId,
    ref: 'UserSchema',
    required: true,
  },
  token: {
      type: String,
      required: true,
  },
});

module.exports = model("VerificationToken", VerificationToken);
