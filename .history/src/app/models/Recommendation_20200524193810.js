const { Schema, model } = require('mongoose');
const bcrypt = require('bcryptjs');

const RecommendationSchema = new Schema(
    {
        client:{
            type: Schema.Types.ObjectId,
            ref:'NewClientSchema',
            required: true,
        },
        professional:{
            type: Schema.Types.ObjectId,
            ref:'NewProfessionalSchema',
            required: true,
        },
        content: {
            type: String,
            maxlength: 250,
        },
        stars: {
            type: Number,
            required: true,
            default: 0,
        },
        client_picture: {
            type: Boolean,
            default: false,
        },
        CreatedAt: {
            type: Date,
            default: Date.now,
        },
    },
);

RecommendationSchema.index({client: 1, professional:1},{unique: true});

module.exports = model("ClientEvaluationSchema", RecommendationSchema); 