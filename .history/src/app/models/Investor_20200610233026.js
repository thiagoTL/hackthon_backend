const { Schema, model } = require("mongoose");
const bcrypt = require("bcryptjs");

const NewClientSchema = new Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
  },
  mobile: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    required: false,
    select: false,
  },
  confirmPassword: {
    type: String,
    required: false,
    select: false,
  },
  birthDate: {
    type: Date,
    required: true,
  },
  linkedin: {
    type: String,
    required: false,
    unique: true,
  },
  country: {
    type: String,
    required: true,
  },
  urls: {
    type: String,
    required: false,
  },
});

NewClientSchema.pre("save", async function (next) {
  const hash = await bcrypt.hash(this.password, 10);
  this.password = hash;

  next();
});

module.exports = model("NewClientSchema", NewClientSchema);
