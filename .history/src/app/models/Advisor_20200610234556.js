const { Schema, model } = require("mongoose");

const NewProfessionalSchema = new Schema({
  user:  {
    type: Schema.Types.ObjectId,
    ref: 'UserSchema',
    required: true,
  },
  assessor: {
    type: Boolean,
    required: false,
  },
  corretora: {
    type: String,
    required: false,
    default: "Independente"
  },
  state: {
    type: String,
    required: false,
  },
  city: {
    type: String,
    required: false,
  },
  avaliacao: {
    sum: {
      type: Number,
      default: 0,
    },
    quantity: {
      type: Number,
      default: 0,
    },
    rating: {
      type: Number,
      default: 0,
    },
  },
  urls: {
    type: String,
    required: false,
  },
  ba: {
    type: Boolean,
    required: false,
    default: false,
  },
  mba: {
    type: Boolean,
    required: false,
    default: false,
  },
  cfa: {
    type: Boolean,
    required: false,
    default: false,
  },
  rfp: {
    type: Boolean,
    required: false,
    default: false,
  },
  cfp: {
    type: Boolean,
    required: false,
    default: false,
  },
  cpa: {
    type: Boolean,
    required: false,
    default: false,
  },
  cim: {
    type: Boolean,
    required: false,
    default: false,
  },
});

NewProfessionalSchema.virtual("url").get(function () {
  const url = process.env.URL || "http://localhost:5000";
  return `${url}/professionals/${encodeURIComponent(this.filesname)}`;
});

module.exports = model("NewProfessionalTesteSchema", NewProfessionalSchema);
