const { mockRequest, mockResponse } = require('/util/Mocks')
const clientController = require('../app/controllers/ClientUserController');


test('Should return a investor and token When create an investor', ()=>{

})

test('Should return an error message When create an investor with an e-mail already registered', ()=>{

})

test('Should return an error message When create an investor with a mobile already registered', ()=>{

})

test('Should return a investor information When show an investor by a correct ID', ()=>{

})

test('Should return an error message When show an investor with an incorrect ID', ()=>{

})

test('Should return a list with all investors When index', ()=>{

})


test('Should return an empty list When index without registered investor yet', ()=>{

})

test('Should return the message *Investor removed successfully.* When remove an investor successfully', ()=>{

})

test('Should return the message *No investor found with the id: $id* When remove an investor with an incorrect ID', ()=>{

})

test('Should return the message *Investor updated successfully.* When update an investor by a correct ID', ()=>{

})

test('Should return the message *No investor found with the id: $id* When update an investor with an incorrect ID', ()=>{

})