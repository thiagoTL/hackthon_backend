const { Router } = require("express");
const multer = require("multer");

// controllers
const Professional = require("./app/controllers/UserProfessionalController");
const Investor = require("./app/controllers/InvestorController");
const Articles = require("./app/controllers/ArticleController");
const UserController = require("./app/controllers/UserController");
const SessionUserController =  require("./app/controllers/SessionUserController");
const RecommendationController = require("./app/controllers/RecommendationController");

const GenerateCode = require("./app/controllers/GenerateCode");
const CertificatesController = require("./app/controllers/CertificatesController");

//middlewares
const authMiddlewares = require("./app/middlewares/auth");
const multerConfig = require("./config/multer");

const routes = new Router();

const uploads = multer(multerConfig);


//user auth
routes.post("/register", UserController.store);
routes.post("/login", SessionUserController.store);

// Código de confirmação de número
routes.post("/code/confirm", GenerateCode.confirm);
routes.post("/code/resend", GenerateCode.resend);

// middleware de autenticação
routes.use(authMiddlewares);

//profissional
routes.get("/advisors", Professional.index);
routes.post("/advisors/qualifications", Professional.show);
routes.delete("/advisors", Professional.remove);

// artigos
routes.get("/articles", Articles.index);
routes.post("/articles/find", Articles.show);
routes.post("/articles/create", Articles.create);
routes.delete("/articles/remove", Articles.remove);
routes.put("/articles/update", Articles.update);
routes.put("/articles/like", Articles.like);

// cliente
routes.get("/investors", Investor.index);
routes.get("/investors/:id", Investor.show);
routes.delete("/investors", Investor.remove);
routes.put("/investors", Investor.update);

//recomendação de cliente
routes.get("/recomms", RecommendationController.index);
routes.post('/recomms', RecommendationController.store);
routes.delete("/recomms", RecommendationController.remove);
routes.put("/recomms", RecommendationController.update);
routes.post("/recomms/find", RecommendationController.show);
routes.delete("/recomms/removeAllByProfessional", RecommendationController.removeAllProfessionalEvaluation);

//certificados
routes.put("/certificates", CertificatesController.update);

module.exports = routes;
