const { Router } = require("express");
const multer = require("multer");

// controllers
const Professional = require("./app/controllers/UserProfessionalController");
const Client = require("./app/controllers/ClientUserController");
const Articles = require("./app/controllers/ArticleController");
const SessionProfessionalController = require("./app/controllers/SessionProfessionalController");
const SessionClientUserController = require("./app/controllers/SessionClientUserController");
const RecommendationController = require("./app/controllers/RecommendationController");

// const GenerateCode = require("./app/controllers/GenerateCode");

const GenerateCode = require("./app/controllers/GenerateCode");
const CertificatesController = require("./app/controllers/CertificatesController");

//middlewares
const authMiddlewares = require("./app/middlewares/auth");
const multerConfig = require("./config/multer");

const routes = new Router();

const uploads = multer(multerConfig);

// profissional auth
routes.post(
  "/register/professional",
  uploads.single("file"),
  Professional.create
);
routes.post("/authenticate/professional", SessionProfessionalController.store);

// client auth
routes.post("/register/client", uploads.single("file"), Client.create);
routes.post("/authenticate/client", SessionClientUserController.store);

// middleware de autenticação
routes.use(authMiddlewares);

//profissional
routes.get("/professionals", Professional.index);
routes.get("/professionals", Professional.show);
routes.delete("/professional", Professional.remove);

// artigos
routes.get("/articles", Articles.index);
routes.get("/articles/:tags", Articles.show);
routes.post("/articles", Articles.create);
routes.delete("/articles/remove", Articles.remove);
routes.put("/articles/update", Articles.update);
routes.put("/articles/like", Articles.like);

// cliente
routes.get("/clients", Client.index);
routes.get("/client/:id", Client.show);
routes.delete("/clients/remove", Client.remove);
routes.put("/clients/update", Client.update);

//recomendação de cliente
routes.get("/recomms", RecommendationController.index);
routes.post('/recomms', RecommendationController.store);
routes.delete("/recomms/remove", RecommendationController.remove);
routes.put("/recomms/update", RecommendationController.update);
routes.get("/recomms/:id", RecommendationController.show);

// Código de confirmação de número
routes.post('/confirm', GenerateCode.show);
//certificados 

routes.put("/certificates", CertificatesController.update);

module.exports = routes;
 