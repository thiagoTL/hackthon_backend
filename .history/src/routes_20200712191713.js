const { Router } = require("express");
const multer = require("multer");

// controllers
const Professional = require("./app/controllers/UserProfessionalController");
const Investor = require("./app/controllers/InvestorController");
const Articles = require("./app/controllers/ArticleController");
const UserController = require("./app/controllers/UserController");
const SessionUserController = require("./app/controllers/SessionUserController");
const RecommendationController = require("./app/controllers/RecommendationController");
const ChatRoomController = require("./app/controllers/ChatRoomController");
const MessageController = require("./app/controllers/MessageController");
const GenerateCode = require("./app/controllers/GenerateCode");
const CertificatesController = require("./app/controllers/CertificatesController");
const EmailSender = require("./app/controllers/EmailSender");

//middlewares
const authMiddlewares = require("./app/middlewares/auth");
const multerConfig = require("./config/multer");

const routes = new Router();

const uploads = multer(multerConfig);

//user auth
routes.post("/register", UserController.store);
routes.post("/login", SessionUserController.store);

// Código de confirmação de número
routes.post("/code/confirm", GenerateCode.confirm);
routes.post("/code/resend", GenerateCode.resend);

// Validação de e-mail 
routes.get("/confirmation/:token", EmailSender.emailConfirmation);
routes.post("/email/resend", EmailSender.resendEmailToken);

// middleware de autenticação
routes.use(authMiddlewares);

// update user - Atualizar dados do usuário logado
routes.put("/user", uploads.single("file"), UserController.update);

//profissional
routes.get("/advisors", Professional.index);
routes.get("/advisors/:id", Professional.show);
routes.post("/advisors/qualifications", Professional.quali);
routes.delete("/advisors", Professional.remove);

// artigos
routes.get("/articles", Articles.index);
routes.post("/articles/find", Articles.show);
routes.post("/articles/create", Articles.create);
routes.delete("/articles/remove", Articles.remove);
routes.put("/articles/update", Articles.update);
routes.put("/articles/like", Articles.like);

// cliente
routes.get("/investors", Investor.index);
routes.get("/investors/:id", Investor.show);
routes.delete("/investors", Investor.remove);
routes.put("/investors", Investor.update);

//recomendação de cliente
routes.get("/recomms", RecommendationController.index);
routes.post("/recomms", RecommendationController.store);
routes.delete("/recomms", RecommendationController.remove);
routes.put("/recomms", RecommendationController.update);
routes.post("/recomms/find", RecommendationController.show);
routes.delete(
  "/recomms/removeAllByProfessional",
  RecommendationController.removeAllProfessionalEvaluation
);

//certificados
routes.put("/certificates", CertificatesController.update);

// chatroom - lista de conversar
routes.get("/chatroom", ChatRoomController.index);
routes.post("/chatroom", ChatRoomController.store);
routes.get("/chatroom/:id", ChatRoomController.show);

// messages - Lista e envio de mensagem
routes.get("/message", MessageController.index);
routes.post("/message", MessageController.store);
routes.get("/message/:id", MessageController.show);

module.exports = routes;
