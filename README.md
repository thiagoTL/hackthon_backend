## Invest Expert Server (Backend Principal)

### 🚧 Invest Expert Server 🚀 em construção... 🚧

## 💻 Sobre o projeto

Desenvolvimento de Backend com nodejs e express. O Projeto que está sendo desenvolvido é um API Rest que vai ser consumida pelo Aplicativo Mobile e o Sistema Web.

## 🛠 Tecnologias

    - node
    - express
    - mongodb

## 🚀 Como executar o projeto

```bash
# clonar este repositório
$ git clone

# instalar as dependências
$ yarn or npm install

# execute o projeto
- Ambiente de Desenvolvimento
    $ yarn dev ou npm run dev

- Ambiente de Produção
    $ yarn start ou npm run start

OBS: TER O DOCKER INSTALADO NA MÁQUINA, CASO TENHA SEGUIR O PASSO A PASSO DO ARQUIVO REAME.md

```

## 📝 Licença

Este projeto esta sobe a licença MIT.

Feito com ❤️ por Invest Expert👋🏽
