## ---- MONGO DB

/_ Conexão Banco de Dados - MongoDB ATLAS _/

mongodb+srv://<username>:<password>@cluster0-4rfre.mongodb.net/test?retryWrites=true&w=majority

## ---- DOCKER MONGODB - CRIANDO CONTAINER

    docker run \
        --name mongodb \
        -e MONGO_INITDB_ROOT_USERNAME=admin \
        -e MONGO_INITDB_ROOT_PASSWORD=1234567 \
        -d \
        mongo:4

--- MONGODB CLIENT
docker run \
 --name mongoclient \
 -p 3000:3036 \
 --link mongodb:mongodb \
 -d \
 mongoclient/mongoclient

## ---- APOS CRIAÇÃO DO CONTAINER, CRIAR USUÁRIO E BASE DE DADOS

docker exec -it mongodb mongo --host localhost -u admin -p 1234567 --authenticationDatabase admin --eval "db.getSiblingDB('investexpert').createUser({ user: 'investexpert', pwd: '123456', roles: [{ role: 'readWrite', db: 'investexpert'}]})"

## ---- Instalando Dependência

npm install
npm start
