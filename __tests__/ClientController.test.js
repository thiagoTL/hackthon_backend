const mongoose = require('mongoose');
const dbHandler = require('./util/MemoryDb');

const { mockRequest, mockResponse } = require('./util/Mocks')
const clientController = require('../app/controllers/ClientUserController');

const request = require('supertest');
const app = require('../app');
/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => await dbHandler.connect());

/**
 * Clear all test data after every test.
 */
afterEach(async () => await dbHandler.clearDatabase());

/**
 * Remove and close the db and server.
 */
afterAll(async () => await dbHandler.closeDatabase());




test('Should return a investor and token When create an investor', async ()=>{
    let req = mockRequest();
    req.params.id = 1;
    const res = mockResponse();
    console.log(clientController.index(req, res));
})

test('Should return an error message When create an investor with an e-mail already registered', ()=>{

})

test('Should return an error message When create an investor with a mobile already registered', ()=>{

})

test('Should return a investor information When show an investor by a correct ID', ()=>{

})

test('Should return an error message When show an investor with an incorrect ID', ()=>{

})

test('Should return a list with all investors When index', async ()=>{
    /**
     * const response = await request(app)
        .get('/clients');

        console.log(response.data);
        expect(response.status).toBe(200);
    */
    
})


test('Should return an empty list When index without registered investor yet', ()=>{

})

test('Should return the message *Investor removed successfully.* When remove an investor successfully', ()=>{

})

test('Should return the message *No investor found with the id: $id* When remove an investor with an incorrect ID', ()=>{

})

test('Should return the message *Investor updated successfully.* When update an investor by a correct ID', ()=>{

})

test('Should return the message *No investor found with the id: $id* When update an investor with an incorrect ID', ()=>{

})

const investorComplete = {

}