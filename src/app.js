require("dotenv").config();

const express = require("express");
const mongoose = require("mongoose");
const io = require("socket.io");
const http = require("http");
const cors = require("cors");
const { resolve } = require("path");
const Sentry = require("@sentry/node");
const Youch = require("youch");

require("express-async-errors");

const routes = require("./routes");
const dbConnection = require("./config/dbConnection");

class Server {
  constructor() {
    this.app = express();
    this.server = http.Server(this.app);

    Sentry.init({ dsn: process.env.SENTRY_DSN });

    this.socket();
    this.database();
    this.middlewares();
    this.routes();
    this.exceptionHandler();

    this.messages = {};
  }

  socket() {
    this.io = io(this.server);

    this.io.on("connection", (socket) => {
      console.log("SOCKET => ", socket);
      
      const { message } = socket.handshake.query;

      this.messages[message] = socket.id;
    });
  }

  middlewares() {
    this.app.use(Sentry.Handlers.requestHandler());
    this.app.use(express.json());
    this.app.use(cors());
    this.app.use(express.static(resolve(__dirname, '..', 'uploads')))
    // this.app.use(
    //   "/files",
    //   express.static(resolve(__dirname, "..", "uploads", "image"))
    // );
    this.app.use((req, res, next) => {
      req.io = this.io;
      req.messages = this.messages;

      next();
    });
  }

  database() {
    mongoose.connect(
      "mongodb://investexpert:1234567@localhost:27017/investexpert",
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: true,
      }
    );

    // mongoose.connect(
    //   "mongodb+srv://hackathon:123qwe@cluster0-4rfre.mongodb.net/hackathongorila?retryWrites=true&w=majority",
    //   {
    //     useNewUrlParser: true,
    //     useUnifiedTopology: true,
    //     useCreateIndex: true,
    //     useFindAndModify: false,
    //   }
    // );
    console.log("CONECTADO");
  }

  routes() {
    this.app.use(routes);
    this.app.use(Sentry.Handlers.errorHandler());

  }

  exceptionHandler() {
    this.app.use(async (err, req, res, next) => {
      const errors = await new Youch(err, req).toJSON();

      return res.status(500).json(errors);
    });
  }
}

module.exports = new Server().server;
