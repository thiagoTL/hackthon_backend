const { Router } = require("express");
const multer = require("multer");
const  swaggerUi = require("swagger-ui-express")

// controllers
const Professional = require("./app/controllers/UserProfessionalController");
const Investor = require("./app/controllers/InvestorController");
const ArticleController = require("./app/controllers/ArticleController");
const UserController = require("./app/controllers/UserController");
const SessionUserController = require("./app/controllers/SessionUserController");
const RecommendationController = require("./app/controllers/RecommendationController");
const ChatRoomController = require("./app/controllers/ChatRoomController");
const MessageController = require("./app/controllers/MessageController");
const GenerateCode = require("./app/controllers/GenerateCode");
const CertificatesController = require("./app/controllers/CertificatesController");
const EmailSender = require("./app/controllers/EmailSenderController");
const ForgotPasswordController = require("./app/controllers/ForgotPasswordController")

//middlewares
const authMiddlewares = require("./app/middlewares/auth");
const multerConfig = require("./config/multer");
const swaggerDocument = require("./config/swagger.json")

const routes = new Router();

const uploads = multer(multerConfig);

routes.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// routes.get("/api-docs", swaggerUi.setup(swaggerDocument))

//user auth
routes.post("/register", UserController.store);
routes.post("/login", SessionUserController.store);

//forgot password
routes.post('/forgot', ForgotPasswordController.store);
routes.post('/forgot/password/:token', ForgotPasswordController.resetPassword)

// Código de confirmação de número
routes.post("/code/confirm", GenerateCode.confirm);
routes.post("/code/resend", GenerateCode.resend);

// Validação de e-mail
routes.get("/confirmation/:token", EmailSender.emailConfirmation);
routes.post("/email/resend", EmailSender.resendEmailToken);

// middleware de autenticação
routes.use(authMiddlewares);

// update user - Atualizar dados do usuário logado
routes.put("/user", uploads.single("file"), UserController.update);

//profissional
routes.get("/advisors", Professional.index);
routes.get("/advisors/:id", Professional.show);
routes.post("/advisors/qualifications", Professional.quali);
routes.delete("/advisors", Professional.remove);

routes.put("/advisors/data", Professional.store);

// artigos
routes.get("/article/:id", ArticleController.index);
routes.post("/article", ArticleController.store);
routes.get("/article/show/:id", ArticleController.show);
routes.delete("/article/:id", ArticleController.delete);
routes.get("/articles", ArticleController.search);

// cliente
routes.get("/investors", Investor.index);
routes.get("/investors/:id", Investor.show);
routes.delete("/investors", Investor.remove);
routes.put("/investors", Investor.update);

//recomendação de cliente
routes.get("/recomms", RecommendationController.index);
routes.post("/recomms/create", RecommendationController.store);
routes.delete("/recomms/remove", RecommendationController.remove);
routes.put("/recomms/update", RecommendationController.update);
routes.post("/recomms/find", RecommendationController.show);
routes.delete(
  "/recomms/removeAllByProfessional",
  RecommendationController.removeAllProfessionalEvaluation
);

//certificados
routes.put("/certificates", CertificatesController.update);

// chatroom - lista de conversar
routes.get("/chatroom", ChatRoomController.index);
routes.post("/chatroom", ChatRoomController.store);
routes.get("/chatroom/:id", ChatRoomController.show);

// messages - Lista e envio de mensagem
routes.get("/message", MessageController.index);
routes.post("/message", MessageController.store);
routes.get("/message/:id", MessageController.show);

module.exports = routes;
