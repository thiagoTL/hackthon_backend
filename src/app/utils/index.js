const twilio = require("twilio");
const sgMail = require("@sendgrid/mail");
  const aws = require("aws-sdk");

const VerificationToken = require("../models/VerificationToken");

const client = twilio(
  process.env.TWILIO_ACCOUNT_SID,
  process.env.TWILIO_AUTH_TOKEN
);

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

module.exports = {
  sendSms: (number) => {
    client.verify
      .services(process.env.TWILIO_SERVICE_SID)
      .verifications.create({ to: number, channel: "sms" })
      .then((verification) => console.log(verification.status))
      .catch((err) => console.log("ERRO", err));
  },
  sendEmail: async (email, id) => {
    try {
      const tokenNumber = Math.floor(Math.random() * 65536);
      const token = {
        userId: id,
        token: tokenNumber,
      };

      await VerificationToken.create(token);

      const msg = {
        to: email,
        from: "info@investexpert.co",
        subject: "Verify Your E-mail",
        text:
          "Hello, \n\nClick on this link to verify your email:\n\nhttps://app.investexpert.co" +
          "/confirmation/" +
          tokenNumber +
          "\n\n" +
          "You will have 12 hours to verify your e-mail before this verification tokens expire.",
        html: `<strong>Click on this link to verify your email:\n\n</strong>
                https://app.investexpert.co/confirmation/${tokenNumber}\n\n
                        You will have 12 hours to verify your e-mail before this verification tokens expire.`,
      };
      await sgMail.send(msg);
    } catch (err) {
      console.log("ERR", err);
    }
  },
  calcMediaReco: (avaliacao, stars) => {
    const sum = avaliacao.sum + stars;
    const qtde = avaliacao.quantity + 1;

    const final = sum != 0 ? sum / qtde : 0;

    return {
      avaliacao: {
        sum: sum,
        quantity: qtde,
        rating: final,
      },
    };
  },

  removeCalcReco: (avaliacao, stars) => {
    const sum = avaliacao.sum - stars;
    const qtde = avaliacao.quantity - 1;

    const final = sum != 0 ? sum / qtde : 0;

    return {
      avaliacao: {
        sum: sum,
        quantity: qtde,
        rating: final,
      },
    };
  },
  updateCalcReco: (avaliacao, oldStars, newStars) => {
    const sum = avaliacao.sum - oldStars + newStars;
    const qtde = avaliacao.quantity;

    const final = sum != 0 ? sum / qtde : 0;

    return {
      avaliacao: {
        sum: sum,
        quantity: qtde,
        rating: final,
      },
    };
  },
  uploadFile: (key, file) => {
    const spacesEndpoint = new aws.Endpoint("sfo3.digitaloceanspaces.com");
    const s3 = new aws.S3({
      endpoint: spacesEndpoint,
      accessKeyId: "243ZM6ZEHSR2U7ZOWIZR",
      secretAccessKey: "Fz6W6J62pEHVkZRqwTP4G11fSx83tchjOjH86dz8Vqw"
    });

    const params = {
      Bucket: "investexpert",
      Key: key,
      Body: file.buffer,
      ACL: "public-read",
      ContentType: file.buffer.type
    };

    s3.putObject(params,(err, data) => {
      if (err) {
        console.log("Upload Failed", err)
        return err
      }else {
        console.log("UPLOAD FILE SUCCESS", data)
        return data
      }    
    });
  }

};
