const AdvisorSchema = require("../models/Advisor");

module.exports = {
  async update(req, res) {
    try {
      const profissional = await AdvisorSchema.findOneAndUpdate(
        { user: req.userId },
        { qualifications: req.body },
        { new: true }
      );

      return res.json("Ok");
    } catch (err) {
      return res.status(400).json({ error: "algo deu errado" });
    }
  },
};
