const AdvisorSchema = require("../models/Advisor");
const UserSchema = require("../models/User");

class ProfessionalController {
  async index(req, res) {
    try {
      const data = await AdvisorSchema.find().populate("user");

      if (data.length == 0) {
        return res
          .status(400)
          .json({ message: "Nenhum profissional cadastrado." });
      }

      return res.send({ data });
    } catch (err) {
      return res.status(400).json({ error: "Algo deu errado :(" });
    }
  }

  async store(req, res){
    try {
      const { company, job, linkedin, years } = req.body;

      const obj = {
        brokerCompany: company,
        job: job,
        linkedin: linkedin,
        yearsExperience: years
      }

      console.log("OBJ => ", obj)
      const response = await AdvisorSchema.findOneAndUpdate({ _id: req.userId }, obj, {
        new: true,
      });

    
      console.log("REQ BODY ->", response)
    }catch(error) {
      console.log("ERRPR => ", error)
    }
  }
  
  async show(req, res) {
    try {
      const response = await AdvisorSchema.findOne({
        user: { _id: req.params.id },
      }).populate("user");

      return res.json(response);
    } catch (error) {
      return res.status(400).json({ error: "Algo deu errado :(" });
    }
  }

  async quali(req, res) {
    const qualifications = req.body;

    try {
      const response = await AdvisorSchema.find({ qualifications }).populate(
        "user"
      );

      return res.json(response);
    } catch (error) {
      return res.status(400).json({ error: "Algo deu errado :(" });
    }
  }

  async remove(req, res) {
    // Apenas o próprio usuário pode se deletar
    try {
      const advisor_setings = await AdvisorSchema.findOneAndRemove({
        user: req.userId,
      });
      const advisor_profile = await UserSchema.findByIdAndRemove({
        _id: req.userId,
      });

      return res.status(204).json({ message: "Successfully removed" });
    } catch (err) {
      return res.status(400).json({ error: "Provider delete failed" });
    }
  }
}
module.exports = new ProfessionalController();
