const ArticleModel = require("../models/Article");

class ArticleController {
  async index(req, res) {
    try {
      const response = await ArticleModel.find({ userId: req.params.id });

      if (response.length === 0) {
        return res.status(400).json({
          error: "There is no article registered, please add a new article.",
        });
      }

      return res.json({ article: response });
    } catch (error) {
      return res
        .status(401)
        .json({ error: "Perform authentication to gain access. " });
    }
  }

  async store(req, res) {
    try {
      const { title, text, tags, posted } = req.body;

      const obj = {
        title: title,
        text: text,
        tags: tags,
        posted: posted,
        userId: req.userId,
      };

      const response = await ArticleModel.create(obj);

      req.io.emit("article", response);

      return res.status(200).json({ article: response });
    } catch (error) {
      return res
        .status(401)
        .json({ error: "Perform authentication to gain access. " });
    }
  }

  async show(req, res) {
    try {
      
      console.log("reqq => ",req.params.id);
      
      const response = await ArticleModel.find({ _id: req.params.id });

      console.log("RESSPONSE => ", response);
      console.log("USERIDD => ",  req.userID)
      if (req.userId == response[0].userId) {
        return res.json({ data: response });
      }

      const responseUpdate = await ArticleModel.findOneAndUpdate(
        { _id: req.params.id },
        { $inc: { views: 1 } }
      )
        .populate("userId")
        .exec();

      const {
        tags,
        views,
        posted,
        _id,
        title,
        text,
        createdAt,
        userId: { url_image, firstName, lastName },
      } = responseUpdate;

      const obj = {
        tags,
        views,
        posted,
        _id,
        title,
        text,
        createdAt,
        url_image,
        firstName,
        lastName,
      };

      return res.json({ data: obj });
    } catch (error) {
      console.log("ERROR  => ", error);
      
           return res
        .status(401)
        .json({ error: "Perform authentication to gain access. " });
    }
  }

  async update(req, res) {
    try {
      return res.json({ ok: true });
    } catch (error) {
      console.log("ERROR +=> ", error);
    }
  }

  async search(req, res) {
    try {
      const response = await ArticleModel.find({ tags: req.query.tag });

      if (response.length === 0) {
        return res
          .state(400)
          .json({ error: "No articles were found during the tag search." });
      }

      return res.status(200).json({ data: response });
    } catch (error) {
      return res
        .status(401)
        .json({ error: "Perform authentication to gain access. " });
    }
  }

  async delete(req, res) {
    try {
      await ArticleModel.findOneAndDelete({
        _id: req.params.id,
      });

      return res.status(201).json({ message: "Article Removed successfully." });
    } catch (error) {
      return res
        .status(401)
        .json({ error: "Perform authentication to gain access. " });
    }
  }
}

module.exports = new ArticleController();
