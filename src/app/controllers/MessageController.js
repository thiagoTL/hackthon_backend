const MessageModel = require("../models/Message");

class MessageController {
  async index(req, res) {
    try {
      const response = await MessageModel.find();

      if (response.length === 0) {
        return res.status(400).json({ message: "There is no message " });
      }

      return res.status(200).json(response);
    } catch (error) {
      return res.status(401).json({ message: "Token Invalid " });
    }
  }

  async store(req, res) {
    try {
      const { message, chatroom, user } = req.body;

      const response = new MessageModel({
        message: message,
        chatroom: chatroom,
        user: user,
      });

      console.log("response", response)

      await response.save();

      req.io.emit("messages", response);

      // console.log('REQ => ', req.io.engine.pingInterval)

      return res.status(201).json(response);
    } catch (error) {
      return res.status(401).json({ message: "Token Invalid " });
    }
  }

  async show(req, res) {
    try {
      const response = await MessageModel.find({
        chatroom: req.params.id,
      });

      return res.status(200).json(response);
    } catch (error) {
      return res.status(401).json({ message: "Token Invalid " });
    }
  }
}

module.exports = new MessageController();
