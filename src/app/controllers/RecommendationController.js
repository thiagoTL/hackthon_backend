const RecommendationSchema = require("../models/Recommendation");
const AdvisorSchema = require("../models/Advisor");
const InvestorSchema = require("../models/Investor");
const RecommCreateValidator = require("../models/validators/RecommValidator");
const RecommUpdateValidator = require("../models/validators/RecommValidator");

const { calcMediaReco, removeCalcReco, updateCalcReco } = require("../utils");

class RecommendationController {
  async store(req, res) {
    const user = req.userId;

    try {
      let client_id;
      await InvestorSchema.findOne({ user }, (err, result) => {
        if (!result) {
          return res.status(400).json({ error: "User is not an investor." });
        }
        client_id = result._id;
      });
      const { professional_id, content, stars, client_picture } = req.body;

      await AdvisorSchema.findOne({ _id: professional_id }, (err, result) => {
        if (!result) {
          return res
            .status(400)
            .json({ error: "Professional Id is not valid" });
        }
      });

      await RecommendationSchema.findOne(
        { client: client_id, professional: professional_id },
        (err, result) => {
          if (result) {
            return res
              .status(400)
              .json({ error: "Recommendation is already registered." });
          }
        }
      );

      const newEvaluation = {
        client: client_id,
        professional: professional_id,
        content: content,
        stars: stars,
        client_picture: client_picture,
      };

      await RecommCreateValidator.validate(newEvaluation);

      const evaluation = await RecommendationSchema.create(newEvaluation);

      if (evaluation != null) {
        const professional = await AdvisorSchema.findOne({
          _id: professional_id,
        });

        if (professional != null) {
          const newAvaliacao = calcMediaReco(professional.avaliacao, stars);

          const professionalUpdated = await AdvisorSchema.findByIdAndUpdate(
            { _id: professional_id },
            newAvaliacao,
            { new: true }
          );

          req.io.emit("recomms", evaluation);

          return res.status(200).json({
            evaluation: evaluation,
            professional: professionalUpdated,
          });
        }
      }
    } catch (err) {
      return res
        .status(400)
        .json({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async update(req, res) {
    const { _id, client_picture, content, stars } = req.body;

    const user = req.userId;
    let client_id;
    await InvestorSchema.findOne({ user }, (err, result) => {
      if (!result) {
        return res.status(400).json({ error: "User is not an investor." });
      }
      client_id = result._id;
    });

    await RecommendationSchema.findOne({ _id: _id }, (err, recomm) => {
      if (!recomm) {
        return res
          .status(400)
          .json({ error: "Recommendation Id is not valid" });
      }
      if (recomm.client != client_id) {
        return res.status(400).json({
          error:
            "Client doesn't have authorization to change this recommendation.",
        });
      }
    });

    const newValues = {
      client_picture: client_picture,
      content: content,
      stars: stars,
    };

    try {
      await RecommUpdateValidator.validate(newValues);
      const evaluation = await RecommendationSchema.findByIdAndUpdate(
        _id,
        newValues,
        { new: true }
      );

      if (evaluation != null) {
        const professional = await AdvisorSchema.findOne(
          evaluation.professional
        );

        if (professional != null) {
          const newAvaliacao = await updateCalcReco(
            professional.avaliacao,
            evaluation.stars,
            stars
          );

          const professionalUpdated = await AdvisorSchema.findByIdAndUpdate(
            professional,
            newAvaliacao,
            { new: true }
          );

          return res.status(200).json({
            professional: professionalUpdated,
          });
        }
      }
    } catch (err) {
      return res
        .status(400)
        .json({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async remove(req, res) {
    const _id = req.body;

    try {
      const recomm = await RecommendationSchema.findOne({ _id: _id });

      if (!recomm) {
        return res
          .status(400)
          .json({ error: "Recommendation Id is not valid" });
      }

      const user = req.userId;
      let client_id;
      await InvestorSchema.findOne({ user }, (err, result) => {
        if (!result) {
          return res.status(400).json({ error: "User is not an investor." });
        }
        client_id = result._id;
      });

      if (recomm.client != client_id) {
        return res.status(400).json({
          error:
            "Client doesn't have authorization to remove this recommendation.",
        });
      }

      const evaluation = recomm;

      await RecommendationSchema.remove(evaluation);
      if (evaluation != null) {
        const professional = await AdvisorSchema.findOne(
          evaluation.professional
        );

        if (professional != null) {
          const newAvaliacao = removeCalcReco(
            professional.avaliacao,
            evaluation.stars
          );

          const professionalUpdated = await AdvisorSchema.findByIdAndUpdate(
            evaluation.professional,
            newAvaliacao,
            { new: true }
          );

          return res.status(200).json({
            professional: professionalUpdated,
          });
        }
      }
    } catch (err) {
      return res
        .status(400)
        .json({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async index(req, res) {
    try {
      const data = await RecommendationSchema.find()
        .populate({ path: "client", populate: { path: "user" } })
        .populate("professional")
        .exec();
      return res.json({ data });
    } catch (err) {
      return res
        .status(400)
        .json({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async show(req, res) {
    const tags = req.body;

    try {
      const recomm = await RecommendationSchema.find(tags)
        .populate({ path: "client", populate: { path: "user" } })
        .populate("professional")
        .exec();
      return res.status(200).json({ data: recomm });
    } catch (err) {
      return res
        .status(400)
        .json({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async removeAllProfessionalEvaluation(req, res) {
    const professional = req.body;

    const cleanEvaluation = { avaliacao: { sum: 0, quantity: 0, rating: 0 } };

    try {
      const professionalUpdated = await AdvisorSchema.findOneAndUpdate(
        professional,
        cleanEvaluation
      );

      await RecommendationSchema.deleteMany({ professional: professional._id });

      return res.staus(200).json(professionalUpdated);
    } catch (err) {
      return res
        .status(400)
        .json({ error: "An error has ocurred. Please, try again. :(" });
    }
  }
}

module.exports = new RecommendationController();
