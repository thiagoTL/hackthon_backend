const NewProfessionalSchema = require("../models/NewProfessional");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const authConfig = require("../../config/auth");

function generateToken(params = {}) {
  return jwt.sign(params, authConfig.secret, {
    expiresIn: authConfig.ttl,
  });
}

/**
 * 
 *  Lembrar de apagar
 * 
 */

class SessionController {
  async store(req, res) {
    const { email, password } = req.body;
    
    const professional = await NewProfessionalSchema.findOne({ email }).select(
      "+password"
    );

    if (!professional) {
      return res.status(400).send({ error: "Usuário não encontrado :( " });
    }

    if (!(await bcrypt.compare(password, professional.password))) {
      return res.status(400).send({ error: "senha incorreta " });
    }

    const { _id, name } = professional;
    professional.password = undefined;

    res.send({
      _id,
      name,
      email,
      token: generateToken({ id: professional.id }),
    });
  }
}

module.exports = new SessionController();
