const UserModel = require("../models/User")
const sgMail = require("@sendgrid/mail")
const bcrypt = require('bcrypt')

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

class ForgotPasswordController {
  async index(req, res) {
    console.log("OK")

    return res.json({ ok: true })
  }

  async store(req, res) {
    try {
      const { email } = req.body;

      const user = await UserModel.findOne({ email: email});

      if(!user) {
        return res.status(400).json({ error: "Email was not found."})
      }

      user.generatePasswordReset();

      user.save();

      const link = `https://5fd1504245ee7300076d1bf5--jovial-liskov-86626c.netlify.app/reset/${user.resetPasswordToken}`;
      const mailOptions = {
        to: user.email,
        from: 'info@investexpert.co',
        subject: 'Password change request',
        text: `Hi ${user.firstName} ${user.lastName} Please click on the following link ${link}`
      }

      sgMail.send(mailOptions, (error, result) => {
          if(error) {
            console.log("ERROR => ", error)
          }

          res.status(200).json({ message: "A reset email has been sent to"})
      })

      return res.json({ message: "Email successfully sent." })
    
    }catch(error) {
      console.log("ERROR => ", error)
    }

    
  }

  async resetToken(req, res) {
    try{
      console.log("REQ => ", req.params);
      // await UserModel.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: {$gt: Date.now()}})
    }catch(error) {
      console.log("ERROR => ", error)
    }
  }

  async resetPassword(req, res) {
    try {
      const { newPassword, confirmPassword } = req.body;
      const { token } = req.params;

      const user = await UserModel.findOne({ resetPasswordToken: token, resetPasswordExpires: {$gt: Date.now()}});

      if(!user) {
        return res.status(401).json({ message: "Password reset token is invalid or has expired" });
      }

      const password = await bcrypt.hash(newPassword, 10);
      const confirmedPassword = await bcrypt.hash(newPassword, 10);

      const id = user._id;

      const obj = {
        password: password,
        confirmedPassword: confirmedPassword
      }

      await UserModel.findOneAndUpdate({ _id: id}, obj)

      const mailOptions = {
        to: user.email,
        from: 'info@investexpert.co',
        subject: 'Your password has been changed',
        text: `Hi ${user.firstName} ${user.lastName}`
      }

      sgMail.send(mailOptions, (error, result ) => {
        if(error) {
          return res.status(500).json({ message: "ERRROR" })
        }

        res.status(200).json({ message: 'Your password has been updated'})
      })

      return res.status(200).json({ message: 'Update Password '})
      
    }catch(error){
      console.log("ERROR => ", error)
    }
  }

}

module.exports = new ForgotPasswordController();