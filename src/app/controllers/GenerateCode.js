const jwt = require("jsonwebtoken");
const twilio = require("twilio");
const UserSchema = require("../models/User");
const authConfig = require("../../config/auth");

const client = twilio(
  process.env.TWILIO_ACCOUNT_SID,
  process.env.TWILIO_AUTH_TOKEN
);

function generateToken(params = {}) {
  return jwt.sign(params, authConfig.secret, {
    expiresIn: 86400,
  });
}

class GenerateCode {
  async confirm(req, res) {
    try {
      const { code, phone } = req.body;

      const user = await UserSchema.find({ mobile: phone });

      if (!user) {
        return res
          .status(400)
          .json({ message: "We were unable to find a user for this mobile." });
      }

      if (user.checkedSms) {
        return res.status(200).json({
          type: "mobile-already-verified",
          message: "This user mobile has already been verified.",
        });
      }

      client.verify
        .services(process.env.TWILIO_SERVICE_SID)
        .verificationChecks.create({ to: phone, code: code })
        .then((verification_check) => res.json(verification_check));

      const checkedSms = true;

      await UserSchema.findOneAndUpdate({
        checkedSms: checkedSms,
      });

      const [
        {
          url_image,
          checkedEmail,
          verifiedIdDocument,
          _id,
          userType,
          firstName,
          lastName,
          email,
          mobile,
          dob,
          country,
          agreeTerms,
        },
      ] = user;

      return res.status(200).json({
        message: "The mobile has been verified.",
        user: {
          url_image,
          checkedSms,
          checkedEmail,
          verifiedIdDocument,
          _id,
          userType,
          firstName,
          lastName,
          email,
          mobile,
          dob,
          country,
          agreeTerms,
        },
        token: generateToken({ id: user._id }),
      });
    } catch (error) {
      return res.status(500).json({ error: error });
    }
  }

  async resend(req, res) {
    try {
      const { phone } = req.body;

      const responseCode = await UserSchema.find({ mobile: phone });

      if (responseCode.length === 0) {
        return res.status(401).json({ message: "User does not exist" });
      }

      if (responseCode.checkedSms) {
        return res
          .status(401)
          .json({ message: "This mobile has already been verified." });
      }

      client.verify
        .services(process.env.TWILIO_SERVICE_SID)
        .verifications.create({ to: phone, channel: "sms" })
        .then((verification) => {
          return res.status(200).json({ message: "Confirmation SMS sent." });
        })
        .catch((err) => {
          return res.status(500).json({ error: "We can't send you the code" });
        });
    } catch (error) {
      return res.status(500).json({ erro: error });
    }
  }
}

module.exports = new GenerateCode();
