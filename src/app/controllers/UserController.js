const bcrypt = require("bcryptjs");
const sharp = require("sharp")
const {resolve, extname} = require("path")
const fs = require("fs")
const crypto = require("crypto")

const UserSchema = require("../models/User");
const UserValidator = require("../models/validators/UserValidator");
const InvestorSchema = require("../models/Investor");
const AdvisorSchema = require("../models/Advisor");

//utils
const { sendSms, sendEmail, uploadFile } = require("../utils");

class UserController {
  async store(req, res) {
    try {
      UserValidator.validate(req.body).catch(function (err) {
        return res.status(400).json({ error: `${err.name}: ${err.errors}` });
      });

      const { email, mobile, userType } = req.body;

      if (await UserSchema.findOne({ email })) {
        return res.status(400).json({ error: "E-mail is already registered." });
      }

      if (await UserSchema.findOne({ mobile })) {
        return res
          .status(400)
          .json({ error: "Mobile number is already registered." });
      }

      const {
        url_image,
        checkedSms,
        checkedEmail,
        verifiedIdDocument,
        _id,
        firstName,
        lastName,
        dob,
        country,
        agreeTerms,
      } = await UserSchema.create(req.body);

      const user = {
        url_image,
        checkedSms,
        checkedEmail,
        verifiedIdDocument,
        _id,
        userType,
        firstName,
        lastName,
        email,
        mobile,
        dob,
        country,
        agreeTerms,
      };

      if (userType === "investor") {
        const investor = await InvestorSchema.create({ user: user._id });

        sendSms(mobile);
        sendEmail(email, user._id);

        return res.json({
          user: user,
          investor: investor,
          message: "Investor registered successfully",
        });
      }

      if (userType === "advisor") {
        const advisor = await AdvisorSchema.create({ user: user._id });

        console.log("ADVISOR =.", advisor);
        sendSms(mobile);
        sendEmail(email, user._id);

        return res.json({
          user: user,
          advisor: advisor,
          message: "Advisor registered successfully",
        });
      }
    } catch (err) {
      console.log("ERRO", err);
      return res
        .status(401)
        .json({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async update(req, res) {
     try {

      const {
        firstName,
        lastName,
        email,
        mobile,
        oldPassword,
        password,
      } = req.body;

      const fileKey =  crypto.randomBytes(16).toString('hex') + extname(req.file.originalname)

      const data = {
        firstName,
        lastName,
        email,
        mobile,
        filename: fileKey
      }

      if(password != '') {
        console.log("ENTRA")
        const passwordV = await bcrypt.hash(password, 8);
        const confirmedPassword = await bcrypt.hash(password, 8);

        const objeto = {
          ...data,
          password: passwordV,
          confirmedPassword
        }

        // função dentro do arquivo util, para realizar upload de arquivos
        uploadFile(fileKey, req.file)

        const response = await UserSchema.findByIdAndUpdate({_id: req.userId}, objeto, { new: true })

         const {
          _id,
          checkedSms,
          checkedEmail,
          verifiedIdDocument,
          userType,
          dob,
          country,
          agreeTerms,
          filename
        } = response

        const obj = {
          _id,
          checkedSms,
          checkedEmail,
          verifiedIdDocument,
          userType,
          firstName,
          lastName,
          email,
          dob,
          country,
          agreeTerms,
          filename
        }

        return res.json(obj)

      }else {
        // função dentro do arquivo util, para realizar upload de arquivos
        uploadFile(fileKey, req.file)

        const response = await UserSchema.findByIdAndUpdate({_id: req.userId}, data, { new: true })

        const {
          _id,
          checkedSms,
          checkedEmail,
          verifiedIdDocument,
          userType,
          dob,
          country,
          agreeTerms,
          filename
        } = response

        const obj = {
          _id,
          checkedSms,
          checkedEmail,
          verifiedIdDocument,
          userType,
          firstName,
          lastName,
          email,
          dob,
          country,
          agreeTerms,
          filename
        }

        return res.json(obj)
      }
      
    } catch (error) {
      return res
        .status(401)
        .json({ error: "An error has ocurred. Please, try again. :(" });
    }
  }

  async index(req, res) {}
}

module.exports = new UserController();
