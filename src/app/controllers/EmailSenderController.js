const UserSchema = require("../models/User");
const VerificationToken = require("../models/VerificationToken");
const sgMail = require("@sendgrid/mail");

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

class EmailSenderController {
  async emailConfirmation(req, res) {
    try {
      const [emailsVerification] = await VerificationToken.find({
        token: req.params.token,
      });

      if (!emailsVerification) {
        return res.status(400).json({
          type: "not verified",
          message:
            "We were unable to find a valid token. Your token may have expired.",
        });
      }

      const user = await UserSchema.findOne({ _id: emailsVerification.userId });

      if (user.checkedEmail) {
        return res.status(400).json({
          message: "This e-mail has already been verified.",
        });
      }

      const checkedEmail = true;

      await UserSchema.findOneAndUpdate({
        checkedEmail: checkedEmail,
      });

      return res
        .status(200)
        .json({ messsage: "The e-mail has been verified. Please log in." });
    } catch (error) {
      return res.status(401).json({ error: "Email invalid" });
    }
  }

  async resendEmailToken(req, res) {
    try {
      const { email } = req.body;

      const [responseUser] = await UserSchema.find({ email: email });

      if (!responseUser) {
        return res.status(401).json({
          message: "We were unable to find a user with that email.",
        });
      }

      if (responseUser.checkedEmail) {
        return res.status(401).json({
          message: "This e-mail has already been verified.",
        });
      }

      const tokenNumber = Math.floor(Math.random() * 65536);
      const token = {
        userId: responseUser._id,
        token: tokenNumber,
      };

      await VerificationToken.create(token);

      const msg = {
        to: email,
        from: "info@investexpert.co",
        subject: "Verify Your E-mail",
        text:
          "Hello, \n\nClick on this link to verify your email:\n\nhttps://app.investexpert.co" +
          "/confirmation/" +
          tokenNumber +
          "\n\n" +
          "You will have 12 hours to verify your e-mail before this verification tokens expire.",
        html: `<strong>Click on this link to verify your email:\n\n</strong>

            https://app.investexpert.co/confirmation/${tokenNumber}\n\n
                        You will have 12 hours to verify your e-mail before this verification tokens expire.`,
      };

      await sgMail.send(msg);

      return res
        .status(201)
        .json({ message: "Email sent, check your email box." });
    } catch (err) {
      return res.staus(500).json({ error: err });
    }
  }
}

module.exports = new EmailSenderController();
