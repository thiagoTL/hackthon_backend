const ChatRoomModel = require("../models/ChatRoom");

class ChatRoomController {
  async index(req, res) {
    try {
      const response = await ChatRoomModel.find();

      if (response.length === 0) {
        return res.status(400).json({ message: "No conversations started." });
      }
      return res.status(200).json(response);
    } catch (error) {
      return res.status(500).json({ error: error });
    }
  }

  async store(req, res) {
    try {

      const chatRoom = await ChatRoomModel.findOne(req.body);

      if (chatRoom) {
        return res.status(200).json(chatRoom);
      } else {
        const response = await ChatRoomModel.create(req.body);

        return res.status(201).json(response);
      }
    } catch (error) {
      console.log("EERROR => ", error)
      return res.status(500).json({ error: error });
    }
  }

  async show(req, res) {
    try {
      const id = req.userId;

      const response = await ChatRoomModel.find({ nameUserAuth: id });

      if (response.length > 0) {
        return res.status(200).json({ data: response });
      }

      const responseChat = await ChatRoomModel.find({ nameChatRoom: id });

      if (responseChat.length > 0) {
        return res.status(200).json({ data: responseChat });
      }

      return res.staus(400).json({ message: "No data found, about messages" });
    } catch (error) {
      return res.status(401).json({ message: "Token Invalid", error: error });
    }
  }
}

module.exports = new ChatRoomController();
