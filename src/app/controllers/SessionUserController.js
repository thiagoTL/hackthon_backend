const UserSchema = require("../models/User");
const VerificationToken = require("../models/VerificationToken");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const authConfig = require("../../config/auth");

function generateToken(params = {}) {
  return jwt.sign(params, authConfig.secret, {
    expiresIn: 86400,
  });
}

class SessionUserController {
  async store(req, res) {
    try {
      const { email, password } = req.body;

      const user = await UserSchema.findOne({ email });

      if (!user) {
        return res.status(400).json({ error: "User not found :( " });
      }

      if (!(await bcrypt.compare(password, user.password))) {
        return res.status(400).json({ error: "Incorrect password" });
      }

      const {
        filename,
        checkedSms,
        checkedEmail,
        verifiedIdDocument,
        _id,
        userType,
        firstName,
        lastName,
        mobile,
        dob,
        country,
        agreeTerms,
      } = user;

      return res.json({
        user: {
          filename,
          checkedSms,
          checkedEmail,
          verifiedIdDocument,
          _id,
          userType,
          firstName,
          lastName,
          email,
          mobile,
          dob,
          country,
          agreeTerms,
        },
        token: generateToken({ id: user.id }),
      });
    } catch (error) {
      return res.status(401).json({ error: "error not invalid" });
    }
  }

}

module.exports = new SessionUserController();
