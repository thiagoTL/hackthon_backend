const InvestorSchema = require("../models/Investor");
const UserSchema = require("../models/User");

class InvestorController {
  async show(req, res) {
    const id = req.params.id;

    try {
      const investor = await InvestorSchema.find({ _id: id }).exec();

      if (!investor) {
        return res
          .status(400)
          .json({ message: "No investor found with the id " });
      }

      const userId = investor[0].user;

      const user = await UserSchema.find({ _id: userId });

      if (!user) {
        return res.status(400).json({ message: "No user found with the id " });
      }

      return res.status(200).json({ investor, user });
    } catch (err) {
      return res.status(400).json({ error: "Investor not found" });
    }
  }

  async index(req, res) {
    try {
      const investors = await InvestorSchema.find().populate("user");

      if (investors.length == 0) {
        return res.status(400).json({ message: "No registered investors." });
      }

      return res.status(200).json(investors);
    } catch (err) {
      return res
        .status(400)
        .json({ error: "An error has ocurred. Please, try again. :( " });
    }
  }

  async remove(req, res) {
    const id = req.body._id;

    try {
      await InvestorSchema.findOneAndRemove({ user: id });
      await UserSchema.findOneAndRemove({ _id: id });

      return res.status(203).json({ message: "Usuário removido com sucesso!" });
    } catch (err) {
      return res.status(400).json({ error: "Usuário não encontrado" });
    }
  }

  async update(req, res) {
    try {
      await UserSchema.findOneAndUpdate({ _id: req.userId }, req.body, {
        new: true,
      });

      return res.status(200).json({ message: "Updated with sucess! :)" });
    } catch (err) {
      return res.status(400).json({ error: "Cannot update" });
    }
  }
}

module.exports = new InvestorController();
