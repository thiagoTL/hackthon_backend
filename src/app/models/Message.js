const { Schema, model } = require("mongoose");

const MessageSchema = new Schema(
  {
    message: {
      type: String,
      required: true,
    },
    chatroom: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: "chatRoomSchema",
    },
    user: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: "UserSchema",
    },
  },
  { timestamps: true }
);

module.exports = model("messageSchema", MessageSchema);
