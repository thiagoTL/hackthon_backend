const { Schema, model } = require("mongoose");

const ChatRoomSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    subName: {
      type: String,
      required: true,
    },
    photoPrimary: {
      type: String,
    },
    photoSecondary: {
      type: String,
    },
    nameUserAuth: {
      type: String,
      required: true,
    },
    nameChatRoom: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

module.exports = model("chatRoomSchema", ChatRoomSchema);
