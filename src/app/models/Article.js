const { model, Schema } = require("mongoose");

const ArticleSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    text: {
      type: String,
      required: true,
    },
    tags: {
      type: Array,
      required: false,
    },
    views: {
      type: Number,
      required: false,
      default: 0,
    },
    posted: {
      type: Boolean,
      required: true,
      default: false,
    },
    userId: {
      type: Schema.Types.ObjectId,
      ref: "UserSchema",
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = model("ArticleSchema", ArticleSchema);
