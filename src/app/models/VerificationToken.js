const { Schema, model } = require("mongoose");

const VerificationToken = new Schema({
  userId:  {
    type: Schema.Types.ObjectId,
    ref: 'UserSchema',
    required: true,
  },
  token: {
      type: String,
      required: true,
  },
  createdAt: { 
    type: Date,
    required: true,
    default: Date.now,
    expires: 43200 }
});

module.exports = model("VerificationToken", VerificationToken);
