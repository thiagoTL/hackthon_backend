const { Schema, model } = require("mongoose");

const InvestorSchema = new Schema({
  user:  {
    type: Schema.Types.ObjectId,
    ref: 'UserSchema',
    required: true,
  },
});

module.exports = model("InvestorSchema", InvestorSchema);
