const { Schema, model } = require('mongoose');

const NewArticleSchema = new Schema(
    {
      user: {
        type: Schema.Types.ObjectId,
        ref: 'NewProfessionalSchema',
        required: true,
      },
      title: {
        type: String,
        required: true,
      },
      body: {
        type: String,
        required: true,

      },
      tags: {
        type: String,
        required: false
      },
      likes: {
        type: Number,
        default: 0,
      },
      CreatedAt: {
        type: Date,
        default: Date.now,
      }
    },
);

module.exports = model("NewArticleSchema", NewArticleSchema);
