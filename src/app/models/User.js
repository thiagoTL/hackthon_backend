const { Schema, model } = require("mongoose");
const bcrypt = require("bcrypt");
const crypto = require('crypto')

const UserSchema = new Schema({
  userType: {
    type: String,
    enum: ["advisor", "investor", "admin"],
    required: true,
  },
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
  },
  confirmedEmail: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
  },
  mobile: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    required: false,
    // select: false,
  },
  confirmedPassword: {
    type: String,
    required: false,
    // select: false,
  },
  dob: {
    type: Date,
    // required: true,
  },
  linkedin: {
    type: String,
    required: false,
    unique: false,
  },
  country: {
    type: String,
    required: true,
  },
  filename: {
    type: String,
    required: false
  },
  agreeTerms: {
    type: Boolean,
    required: true,
  },
  checkedSms: {
    type: Boolean,
    default: false,
  },
  checkedEmail: {
    type: Boolean,
    default: false,
  },
  verifiedIdDocument: {
    type: Boolean,
    required: false,
    default: false,
  },
  resetPasswordToken: {
    type: String,
    required: false
  },
  resetPasswordExpires: {
    type: Date,
    required: false
  }
});

UserSchema.pre("save", async function (next) {
  if (this.password) {
    const hash = await bcrypt.hash(this.password, 8);
    const hashConfirm = await bcrypt.hash(this.confirmedPassword, 8);

    this.password = hash;
    this.confirmedPassword = hashConfirm;
  }

  next();
});

UserSchema.methods.generatePasswordReset = function() {
  this.resetPasswordToken = crypto.randomBytes(20).toString('hex');
  this.resetPasswordExpires = Date.now() + 3600000;
}

UserSchema.virtual("url").get(function() {
  const url = process.env.URL || "http://localhost:5000";

  return `${url}/user/${encodeURIComponent(this.path)}`
})
// userSchema.pre('save', async function () {
//   if (this.isModified('password')) {
//     this.password = await User.hash(this.password)
//   }
// })
// userSchema.statics.hash = function(password) {
//   return bcrypt.hash(password, 10)
// }
// userSchema.methods.matchesPassword = function(this, password) {
//   return bcrypt.compare(password, this.password)
// }
// // ...
// const user = await User.findOne({ email })
// if (!(await user.matchesPassword(password))) {
//     throw new Error('Senha Incorreta')
// }

module.exports = model("UserSchema", UserSchema);
